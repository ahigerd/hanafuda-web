# hanafuda-web

## Dependencies
* Python 3.6 or higher
* pipenv

## Building
* `make`

## Running server
* `make run`

hanafuda-web is also a WSGI application and can be hosted on any non-forking server.

## Copyright

hanafuda-web is copyright (c) 2020 Adam Higerd.

hanafuda-web is licensed under the [MIT license](LICENSE.md).

The "Default" card graphics are derived from
[SheldonSChen's hanafuda app](https://github.com/SheldonSChen/hanafuda),
copyright (c) 2020 Sheldon Chen, which are
[distributed freely for non-commercial use](https://github.com/SheldonSChen/hanafuda/issues/6).

The "Louie Mantia (recolor)" card graphics were created by
[Louie Mantia](https://www.junior.cards/about) and adapted by
[AlexisDot](https://github.com/AlexisDot/Hanafuda-Louie-Recolor).
Copyright (c) 2021, licensed under the
[Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

The "Ry's Pixel Hanafuda" card graphics were created by
Ryan Sartor. Copyright (c) 2020, licensed under the
[Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](http://creativecommons.org/licenses/by-nc-nd/4.0/).
