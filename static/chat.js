import AutoUI, { DialogUI } from './autoui.js';
import srv from './hanafuda.js';
import { aboutDialog, promptUsername, settingsDialog } from './dialogs.js';

class ChatUIClass extends AutoUI {
  constructor() {
    super(document.getElementById('chat'));
    this.settings.onclick = settingsDialog;
    this.chatform.onsubmit = this.onSubmit.bind(this);
    this.setPlayerName = this.setPlayerName.bind(this);
    this.notifyMemberList = this.notifyMemberList.bind(this);
    srv.addHandler('message', this.addMessage.bind(this));
    srv.addHandler('renamed', this.notifyMemberList);
    srv.addHandler('spectating', this.notifyMemberList);
    srv.addHandler('joined', this.notifyMemberList);
    srv.addHandler('left', this.notifyMemberList);
    this.playername.addEventListener('click', this.promptUsername.bind(this, false));

    this.playername.innerText = '';
  }

  async init() {
    try {
      if (localStorage.chatUsername) {
        await this.setPlayerName(localStorage.chatUsername, false);
      } else {
        this.playername.innerText = '(anonymous)';
      }
    } catch (err) {
      this.playername.innerText = '(anonymous)';
      localStorage.chatUsername = '';
      console.error(err);
    }
  }

  get playerName() {
    return localStorage.chatUsername;
  }

  async setPlayerName(name, displayError = true) {
    try {
      await srv.call('set_name', { name })
      this.playername.innerText = name || '(anonymous)';
      localStorage.chatUsername = name;
      this.onNameUpdated();
    } catch (err) {
      if (displayError) {
        await DialogUI.push(`The name "${name}" was not accepted.`, ['OK']).promise();
      }
      this.playername.innerText = localStorage.chatUsername || '(anonymous)';
      throw err;
    }
  }

  async promptUsername(required = false) {
    const name = await promptUsername(required);
    if (name || required) {
      await this.setPlayerName(name);
    }
  }

  addLine(element) {
    if (typeof element == 'string') {
      const div = document.createElement('DIV');
      div.innerHTML = element;
      element = div;
    }
    this.chatbuffer.appendChild(element);
    this.chatbuffer.scrollTop = this.chatbuffer.scrollHeight;
  }

  onSubmit(event) {
    event.preventDefault();
    srv.emit('message', { msg: this.chatmessage.value });
    this.chatmessage.value = '';
    this.chatmessage.focus();
    return false;
  }

  addMessage(data) {
    const div = document.createElement('DIV');
    div.innerHTML = '<b>&lt;' + data.src + '&gt;</b> ' + data.msg;
    if (data.src == 'System') {
      div.className = 'systemMessage';
    }
    this.addLine(div);
  }

  notifyMemberList(data) {
    const div = document.createElement('DIV');
    let message = ' has ' + data.eventName + ' the room.';
    if (data.eventName == 'renamed') {
      message = ' is now known as ' + data.newName + '.';
    } else if (data.eventName == 'spectating') {
      message = ' has started spectating.';
    }
    div.innerText = '* ' + data.name + message;
    div.className = 'joinMessage';
    this.chatbuffer.appendChild(div);
    this.chatbuffer.scrollTop = this.chatbuffer.scrollHeight;
  }

  onNameUpdated() {
    // no-op by default
  }
}

const ChatUI = new ChatUIClass();
export default ChatUI;
