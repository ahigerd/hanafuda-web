class AnimationQueue {
  constructor() {
    this._queue = [];
    this._promise = null;
    this._finished = this._finished.bind(this);
    this._finishedErr = this._finishedErr.bind(this);
  }

  get isAnimating() {
    return !!this._promise;
  }

  _play(fn) {
    this._promise = Promise.resolve().then(fn).then(this._finished, this._finishedErr);
  }

  enqueue(fn) {
    if (this.isAnimating) {
      this._queue.push(fn);
    } else {
      this._play(fn);
    }
  }

  _finished() {
    const nextFn = this._queue.shift();
    if (nextFn) {
      this._play(nextFn);
    } else {
      this._promise = null;
    }
  }

  _finishedErr(err) {
    console.error(err);
    return this._finished();
  }
}

const animQ = new AnimationQueue();
export default animQ;
