export const urlPrefix = `${location.pathname}`.replace(/\/(?:lobby|room).*$/, '').replace(/#.*$/, '');
export const cookies = {};

let cardsetPath = 'cards';
let cardsetFormat = 'svg';
let _onLoadCardset = null;

export function onLoadCardset(callback) {
  _onLoadCardset = callback;
}

export function cardImageURL(month, num=-1) {
  if (num < 0) {
    num = 3 - (month % 4);
    month = Math.floor(month / 4) + 1;
  }
  const cardset = localStorage.cardset || 'cards';
  return `${urlPrefix}/static/${cardsetPath}/${month}${num}.${cardsetFormat}`;
}

function preloadImages() {
  let html = '';
  let head = [];
  for (let i = 0; i < 48; i++) {
    const preload = document.createElement('LINK');
    preload.href = cardImageURL(i);
    preload.rel = 'preload';
    preload.as = 'image';
    document.head.appendChild(preload);
    html += `<img src='${cardImageURL(i)}' />`;
  }
  /*
  for (const sound of ['clack']) {
    const url = `${urlPrefix}/static/${sound}.mp3`;
    html += `<audio id='snd_${sound}' preload='auto' src='${url}'></audio>`;
  }
  */
  document.getElementById('preload').innerHTML = html;
}

export function loadCardsetJSON(cardset) {
  return fetch(`${urlPrefix}/static/${cardset}.set.json`).then(res => res.json());
}

let lastCardset = null;
export async function loadCardset(cardset) {
  try {
    const data = await loadCardsetJSON(cardset);
    cardsetPath = data.cards;
    cardsetFormat = data.format || 'svg';
    localStorage.cardset = cardset;

    if (lastCardset) {
      document.body.classList.remove(lastCardset);
    }
    document.body.classList.add(cardset);
    lastCardset = cardset;

    const oldStyle = document.getElementById('cardsetStyle');
    if (oldStyle) {
      oldStyle.remove();
    }

    if (data.css) {
      const newStyle = document.createElement('LINK');
      newStyle.href = `${urlPrefix}/style/${data.css.replace('scss', 'css')}`;
      newStyle.rel = 'stylesheet';
      newStyle.type = 'text/css';
      newStyle.id = 'cardsetStyle';
      document.head.appendChild(newStyle);
    }
  } catch (err) {
    console.error(err);
    cardsetPath = 'cards';
  }
  preloadImages();
  for (const div of document.querySelectorAll('div.card:not(.anchor) div[style]')) {
    const bg = div.style.backgroundImage;
    if (bg.startsWith('url(')) {
      for (const cls of div.classList.values()) {
        if (cls.startsWith('card-')) {
          const cardId = Number(cls.split('-')[1]);
          div.style.backgroundImage = `url("${cardImageURL(cardId)}")`;
          break;
        }
      }
    }
  }
  if (_onLoadCardset) {
    _onLoadCardset();
  }
}
loadCardset(localStorage.cardset || 'default');

export function delay(timeout) {
  return new Promise(resolve => window.setTimeout(resolve, timeout));
}

export function setCookie(name, value) {
  const hostname = window.location.host.split(':')[0];
  cookies[name] = value;
  document.cookie = `${name}=${value};path=${urlPrefix};samesite=strict;domain=${hostname}`;
}

(function ensureCookie() {
  let id = '';
  for (const cookie of (document.cookie || '').split(';')) {
    const comma = cookie.indexOf('=');
    const key = cookie.substr(0, comma).trim();
    const value = cookie.substr(comma + 1).trim();
    cookies[key] = value;
    if (key == 'playerID') {
      id = cookie.substr(comma + 1).trim();
    }
  }
  if (!id) {
    id = btoa(`${Math.random()}!${Date.now() + performance.now()}`);
    const hostname = window.location.host.split(':')[0];
    if (hostname == 'localhost') {
      window.location.replace(window.location.href.replace('//localhost', '//127.0.0.1'));
      return;
    }
    setCookie('playerID', id);
  }
})();
