import srv from './hanafuda.js';
import AutoUI, { DialogUI } from './autoui.js';
import ChatUI from './chat.js';
import { urlPrefix, cookies, setCookie, loadCardset, loadCardsetJSON } from './util.js';

export async function promptUsername(required = false) {
  let buttons;
  if (required) {
    buttons = "<input type='submit' value='Enter Game' />";
  } else {
    buttons = "<input type='submit' value='Save' />";
    if (localStorage.chatUsername) {
      buttons += " <input type='button' class='cancel' value='Cancel' />";
    }
  }
  const htmlUsername = (localStorage.chatUsername || '').replace('"', '&quot;');
  const html = `<form>
    Please enter your name:<br/>
    <p style='margin:1em'><input name='username' value="${htmlUsername}" /></p>
    ${buttons}
  </form>`;
  const dlg = DialogUI.push(html);
  await dlg.promise();
  return dlg.username.value;
}

export function aboutDialog() {
  const html = `<div style="text-align:center;font-size:150%;font-weight:bold;margin-bottom:1em">hanafuda-web</div>
    <div style="max-width: 800px">
      <p>Copyright &copy; 2020-2021 Adam Higerd (chighland@gmail.com)</p>
      <p>
        Permission is hereby granted, free of charge, to any person obtaining a copy
        of this software and associated documentation files (the "Software"), to deal
        in the Software without restriction, including without limitation the rights
        to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
        copies of the Software, and to permit persons to whom the Software is
        furnished to do so, subject to the following conditions:
      </p>
      <p>
        The above copyright notice and this permission notice shall be included in all
        copies or substantial portions of the Software.
      </p>
      <p>
        THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
        IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
        FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
        AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
        LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
        OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
        SOFTWARE.
      </p>
    </div>
    <div style="text-align: center"><input type='button' value='Close' class='cancel' /></div>
  `;
  DialogUI.push(html);
}

async function aboutCardset(dlg) {
  const cardset = dlg.cardsetSelect.value;
  const data = await loadCardsetJSON(cardset);
  dlg.cardsetName.innerText = data.name;
  dlg.copyrightYear.innerText = data.year;
  dlg.copyrightName.innerText = data.credit;
  if (data.licenseLink) {
    dlg.cardLicense.href = data.licenseLink;
  }
  dlg.cardLicense.innerText = data.license;
  dlg.cardAbout.href = data.link;
}

export async function settingsDialog() {
  const html = await (fetch(urlPrefix + '/settings').then(response => response.text()));
  const dlg = DialogUI.push(html);
  dlg.aboutSettings.onclick = aboutDialog;
  dlg.setUsername.value = ChatUI.playerName;
  dlg.themeSelect.value = cookies.hf_theme || 'default';
  dlg.cardsetSelect.value = localStorage.cardset || 'default';
  dlg.cardsetSelect.addEventListener('change', () => aboutCardset(dlg));
  let shouldReload = false;
  dlg.onSubmit = async () => {
    if (dlg.themeSelect.value != cookies.hf_theme) {
      setCookie('hf_theme', dlg.themeSelect.value);
      shouldReload = true;
    }
    if (dlg.cardsetSelect.value != localStorage.cardset) {
      if (shouldReload) {
        localStorage.cardset = dlg.cardsetSelect.value;
      } else {
        loadCardset(dlg.cardsetSelect.value);
      }
    }
    if (ChatUI.playerName != dlg.setUsername.value) {
      try {
        await ChatUI.setPlayerName(dlg.setUsername.value);
      } catch (err) {
        dlg.setUsername.value = ChatUI.playerName;
        return;
      }
    }
    if (shouldReload) {
      window.location.reload();
    }
  };
  aboutCardset(dlg);
  return dlg;
}
