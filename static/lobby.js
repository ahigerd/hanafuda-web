import { urlPrefix } from './util.js';
import AutoUI from './autoui.js';
import srv from './hanafuda.js';
import ChatUI from './chat.js';
import { aboutDialog } from './dialogs.js';

const LobbyUI = new AutoUI(document.getElementById('lobby'));
window.LobbyUI = LobbyUI;

let listThrottle = null;
const roomDivs = {};

class RoomDiv {
  constructor(room) {
    this.id = room.id;
    this.div = document.createElement('DIV');
    this.div.id = `room_${this.id}`;
    this.div.className = 'room';
    this.update(room);
  }

  dispose() {
    delete roomDivs[this.id];
    if (this.div && this.div.parentElement) {
      this.div.parentElement.removeChild(this.div);
    }
  }

  playerHTML(index) {
    let html = `<b>Player ${index + 1}:</b> `;
    if (!this.players[index]) {
      return html + '<i>Waiting...</i><br/>';
    }
    html += this.players[index];
    if (this.status == 'playing') {
      html += ` (${this.scores[index]} point${this.scores[index] != 1 ? 's' : ''})`;
    }
    return html + '<br/>';
  }

  winner() {
    if (this.scores[0] == this.scores[1]) {
      return 'Draw';
    } else if (this.scores[0] > this.scores[1]) {
      return this.players[0];
    } else {
      return this.players[1];
    }
  }

  statusHTML() {
    switch (this.status) {
      case 'waiting': return 'Waiting for players...';
      case 'playing': return `<b>Month:</b> ${this.month}`;
      case 'gameOver': return `<b>Last winner:</b> ${this.winner()}`;
      default: return 'Not playing';
    }
  }

  update(room) {
    console.log(room)
    if (room.hide) {
      this.dispose();
      return;
    }
    this.players = room.players;
    this.scores = room.scores;
    this.inRoom = room.inRoom;
    this.month = room.month;
    this.ruleset = room.ruleset;
    this.spectators = room.spectators;

    if (this.players.length < 2) {
      this.status = 'waiting';
    } else if (this.month < 0) {
      this.status = 'notPlaying';
    } else if (this.month > this.ruleset.months) {
      this.status = 'gameOver';
    } else {
      this.status = 'playing';
    }


    let buttons = '';
    const roomURL = `${urlPrefix}/room/${room.id}`;
    if (room.inRoom) {
      buttons = `<a href='${roomURL}'>Open</a>`;
    } else {
      if (room.players.length < 2) {
        buttons = `<a href='${roomURL}/join'>Join</a> `;
      }
      buttons += `<a href='${roomURL}/spectate'>Spectate</a>`;
    }

    this.div.innerHTML = `<h3>Room #${room.id}</h3>` +
      this.playerHTML(0) +
      this.playerHTML(1) +
      `<b>Spectators:</b> ${room.spectators.length}<br/>
      <p>${this.statusHTML()}</p>
      <div>${buttons}</div>`;
  }
}

function updateRoom(room) {
  if (room.room) {
    room = room.room;
  }
  if (room.id in roomDivs) {
    roomDivs[room.id].update(room);
  } else if (!room.hide) {
    roomDivs[room.id] = new RoomDiv(room);
    LobbyUI.roomlist.appendChild(roomDivs[room.id].div);
  }
}

function updateList(rooms) {
  if (rooms.rooms) {
    rooms = rooms.rooms;
  }
  for (const room of rooms) {
    updateRoom(room);
  }
  for (const roomId of Object.keys(roomDivs).filter(id => !rooms.find(r => +r.id == +id))) {
    roomDivs[roomId].dispose();
  }
}

function queryList(throttle = 1000) {
  if (listThrottle) {
    if (throttle) {
      return;
    } else {
      window.clearTimeout(listThrottle);
    }
  }
  listThrottle = window.setTimeout(
    async function() {
      const list = await srv.call('list');
      listThrottle = null;
      updateList(list);
    },
    throttle,
  );
}

srv.addHandler('list', updateList);
srv.addHandler('update', updateRoom);
ChatUI.leave.style.visibility = 'hidden';
LobbyUI.aboutButton.onclick = aboutDialog;

(async function() {
  await ChatUI.init();
  ChatUI.onNameUpdated = () => queryList(0);
  queryList(0);
  LobbyUI.lobby.style.display = '';
})();
