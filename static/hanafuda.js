import './util.js';

class RemoteError extends Error {
  constructor(data) {
    super(data.error);
    Object.assign(this, data);
  }
}

const baseURL = `${location.href}`.replace(/static.*$/, '').replace(/\/$/, '');
let requestIndex = 0;
const outstandingRequests = new Map();

export class EventPoller {
  constructor() {
    this.lastTimestamp = 0;
    this.baseRetryInterval = 5000;
    this.maxRetryInterval = 1000 * 60 * 5; // 5 minutes
    // TODO: if retry interval exceeds a threshold, show a "reconnecting..." message
    this.retryInterval = this.baseRetryInterval;
    this.handlers = {};
    this.dispatch = this.dispatch.bind(this);
    this.connect = this.connect.bind(this);
    this.websocket = null;
    this.stopped = false;
  }

  connect() {
    if (this.stopped) {
      return Promise.reject();
    } else if (!this.websocket || this.websocket.readyState == WebSocket.CLOSING || this.websocket.readyState == WebSocket.CLOSED) {
      const wsUrl = window.location.origin.replace(/^http/, 'ws') + window.location.pathname;
      this.websocket = new WebSocket(wsUrl);
      this.websocket.addEventListener('message', this.dispatch);
    } else if (this.websocket.readyState == WebSocket.OPEN) {
      return Promise.resolve();
    }
    // TODO: auto-reconnect
    return new Promise((resolve, reject) => {
      this.websocket.addEventListener('open', resolve, { once: true });
      this.websocket.addEventListener('error', reject, { once: true });
    });
  }

  stop() {
    this.stopped = true;
    if (this.websocket) {
      this.websocket.close();
    }
  }

  async emit(dataOrFunc, data = null) {
    if (!data) {
      data = dataOrFunc;
    } else if (typeof(dataOrFunc) === 'string') {
      data['_func'] = dataOrFunc;
    }
    await this.connect();
    this.websocket.send(JSON.stringify(data));
  }

  dispatch(data) {
    if (data instanceof MessageEvent) {
      data = JSON.parse(data.data);
    }
    if (data && data._id) {
      const cb = outstandingRequests.get(data._id);
      outstandingRequests.delete(data._id);
      if (data.error) {
        cb.reject(new RemoteError(data));
      } else {
        cb.resolve(data);
      }
      return;
    }
    if (!data || !data.timestamp) {
      throw new Error('Bad event: ' + JSON.stringify(data));
    }
    if (this.lastTimestamp < data.timestamp) {
      this.lastTimestamp = data.timestamp;
    }
    if (!data.eventName || data.eventName == 'noop') {
      // no-op event for timestamp synchronization
      return;
    }
    const handlers = this.handlers[data.eventName];
    if (!handlers || !handlers.length) {
      console.warn('Unknown event', data);
      return;
    }
    for (const handler of handlers) {
      try {
        handler(data);
      } catch (err) {
        console.error(err);
      }
    }
  }

  addHandler(eventName, callback) {
    if (!(eventName in this.handlers)) {
      this.handlers[eventName] = [];
    }
    this.handlers[eventName].push(callback);
  }

  call(_func, params = null, options = {}) {
    let fail = null;
    const runner = new Promise((resolve, reject) => {
      fail = reject;
      ++requestIndex;
      outstandingRequests.set(requestIndex, { resolve, reject });
      srv.emit({
        ...params,
        _func,
        _id: requestIndex,
      });
    });
    if (options.timeout) {
      return Promise.race([delay(options.timeout).then(fail), runner]);
    }
    return runner;
  }
}

export const srv = new EventPoller();
srv.addHandler('ping', () => srv.emit({ '_func': 'pong' }));
srv.connect();
export default srv;
