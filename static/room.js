import { urlPrefix, cardImageURL, onLoadCardset, delay } from './util.js';
import AutoUI, { DialogUI } from './autoui.js';
import { srv } from './hanafuda.js';
import ChatUI from './chat.js';
import animQ from './animq.js';
import { promptUsername } from './dialogs.js';

const UI = new AutoUI(document.getElementById('game'));
let idleDlg = null, koikoiDlg = null;
let disableAnimations = false;
let lastKoikoiData = null;

const HanafudaCards = {};
const zones = {
  myhand: [],
  mymatches: [],
  opphand: [],
  oppmatches: [],
  field: [],
  deckzone: [],
};
let ruleset = null;
let cardGroups = {};
let userList = [];
let pendingRemove = [];
let pendingDeal = true;

let gameState = 'wait';
let playerNumber = 0;
let currentPlayer = null;
let currentMonth = 0;
let selectedCard = null;
let faceup = null;

function isPlayableState() {
  if (currentPlayer !== playerNumber) {
    return false;
  }
  switch (gameState) {
    case 'draw':
    case 'play':
      return gameState;
    default:
      return false;
  }
}

class HanafudaCard {
  static get(index, noAnimate = false) {
    if (!(index in HanafudaCards)) {
      HanafudaCards[index] = new HanafudaCard(index, noAnimate);
    }
    return HanafudaCards[index];
  }

  static updatePositions(zone = null) {
    for (const card of (zone || Object.values(HanafudaCards))) {
      card.updatePosition(true);
    }
  }

  constructor(index, noAnimate) {
    this.noAnimate = noAnimate;
    this.updatePosition = this.updatePosition.bind(this);
    this.index = index;
    this.zone = null;
    this.anchor = document.createElement('DIV');
    this.anchor.className = 'card anchor card_' + index;
    this.anchor.addEventListener('mouseenter', this.updatePosition);
    this.anchor.addEventListener('mouseleave', this.updatePosition);
    this.anchor.addEventListener('touchstart', this.updatePosition);
    this.anchor.addEventListener('touchend', this.updatePosition);
    this.element = document.createElement('DIV');
    this.element.className = 'card';
    this.element.card = this;
    this.anchor.appendChild(this.element);
    if (typeof(index) == 'number' && index >= 0) {
      this.num = 3 - (index % 4);
      this.month = Math.floor(index / 4) + 1;
      this.element.innerHTML = `<div class='card-${this.index}' style='background-image:url(${cardImageURL(this.month, this.num)})'></div>`;
      this.faceDown = false;
    } else {
      this.faceDown = true;
    }
    this.element.addEventListener('click', this.onClick.bind(this));
    this.element.addEventListener('mouseenter', this.onMouseEnter.bind(this));
    this.element.addEventListener('mouseleave', this.onMouseLeave.bind(this));
  }

  updatePosition(recursive = false) {
    if (!recursive && this.zone) {
      for (const card of zones[this.zone]) {
        card.updatePosition(true);
      }
      return;
    }

    if (!this.isVisible) {
      if (this.element.parentElement) {
        this.element.parentElement.removeChild(this.element);
      }
      return;
    } else if (!this.element.parentElement) {
      this.anchor.appendChild(this.element);
    }
    let left = 0, top = 0, anchor = this.anchor;
    while (anchor) {
      left += anchor.offsetLeft;
      top += anchor.offsetTop;
      anchor = anchor.offsetParent;
    }
    this.element.style.left = left + 'px';
    this.element.style.top = top + 'px';
  }

  _reset() {
    if (this.zone) {
      const pos = zones[this.zone].indexOf(this);
      if (pos >= 0) {
        zones[this.zone].splice(pos, 1);
      }
    }
    this.zone = null;
    this.selected = false;
    this.enabled = false;
    this.disabled = false;
  }

  _assign(container) {
    container.appendChild(this.anchor);
    if (this !== placeholder && container == UI.field) {
      fixPlaceholder();
    }
    if (container.card) {
      this.zone = container.card.zone;
    } else {
      this.zone = container.id || container.parentElement.id;
    }
    if (this.zone) {
      zones[this.zone].push(this);
    }
  }

  addTo(container, runMode = 'queue') {
    const removeIndex = pendingRemove.indexOf(this);
    if (removeIndex >= 0) {
      pendingRemove.splice(removeIndex, 1);
    }

    const anim = () => {
      if (container == this.anchor.parentElement) {
        return;
      }

      const resetZone = this.zone;
      this._reset();
      this._assign(container);
      this.anchor.appendChild(this.element);
      this.updatePosition();
      if (resetZone) {
        HanafudaCard.updatePositions(zones[resetZone]);
      }
    };
    if (runMode == 'queue' || runMode == 'coalesce') {
      animQ.enqueue(anim);
    } else {
      return anim();
    }
  }

  moveTo(container, runMode = 'queue') {
    const removeIndex = pendingRemove.indexOf(this);
    if (removeIndex >= 0) {
      pendingRemove.splice(removeIndex, 1);
    }

    const anim = () => {
      this._reset();
      if (this.anchor.parentElement) {
        this.anchor.parentElement.removeChild(this.anchor);
      }
      this._assign(container);
      let promise;
      if (!this.noAnimate && !disableAnimations && !document.hidden) {
        this.element.classList.add('animate');
        promise = new Promise(resolve => this.element.addEventListener('transitionend', resolve, { once: true }))
          .then(() => this.element.classList.remove('animate'));
      } else {
        promise = Promise.resolve();
      }
      this.updatePosition();
      return runMode == 'coalesce' ? Promise.resolve() : promise;
    };
    if (runMode == 'queue' || runMode == 'coalesce') {
      animQ.enqueue(anim);
    } else {
      return anim();
    }
  }

  remove() {
    this._reset();
    if (this.anchor.parentElement) {
      this.anchor.parentElement.removeChild(this.anchor);
    }
    if (this.element.parentElement) {
      this.element.parentElement.removeChild(this.element);
    }
    this.updatePosition();
  }

  onClick(event) {
    event.preventDefault();
    if (this.enabled && isPlayableState()) {
      if (gameState == 'play') {
        matchCardFromHand(this);
      } else if (gameState == 'draw') {
        matchCardFromDeck(this);
      }
    }
    return false;
  }

  onMouseEnter(event) {
    if (isPlayableState() && this.zone == 'field' && selectedCard && this.month == selectedCard.month) {
      const thisMonth = [];
      for (const card of zones.field) {
        if (card.month == this.month) {
          thisMonth.push(card);
        }
      }
      if (thisMonth.length == 3) {
        for (const card of thisMonth) {
          card.highlight = true;
        }
      } else {
        clearHighlights();
      }
    } else if (this.zone == 'mymatches' || this.zone == 'oppmatches') {
      HanafudaCard.updatePositions(zones[this.zone]);
    }
  }

  onMouseLeave(event) {
    clearHighlights();
    if (this.zone == 'mymatches' || this.zone == 'oppmatches') {
      setTimeout(() => HanafudaCard.updatePositions(zones[this.zone]), 16);
    }
  }

  get isVisible() { return this.anchor && !!this.anchor.offsetParent; }

  get enabled() { return this.anchor.parentElement && (this.element.classList.contains('enabled') || this.anchor.parentElement.classList.contains('enabled')); }
  set enabled(on) { this.element.classList.toggle('enabled', on); }

  get disabled() { return this.element.classList.contains('disabled'); }
  set disabled(on) { this.element.classList.toggle('disabled', on); }

  get highlight() { return this.element.classList.contains('highlight'); }
  set highlight(on) { this.element.classList.toggle('highlight', on); }

  get selected() { return this.element.classList.contains('selected'); }
  set selected(on) { this.element.classList.toggle('selected', on); }

  setLabel(label) {
    this.element.innerText = label;
  }
}

function fixPlaceholder() {
  if (!placeholder.anchor.parentElement || placeholder.anchor.nextSibling) {
    placeholder.zone = 'field';
    UI.field.appendChild(placeholder.anchor);
  }
}

function clearHighlights() {
  for (const el of document.querySelectorAll('.highlight')) {
    el.classList.remove('highlight');
  }
}

function clearSelection() {
  if (selectedCard) {
    selectedCard.selected = false;
    selectedCard = null;
  }
}

function fillZone(cards, element) {
  if (Array.isArray(cards)) {
    for (const index of cards) {
      HanafudaCard.get(index).addTo(element, 'now');
    }
  } else {
    for (let i = 0; i < cards; i++) {
      HanafudaCard.get(`${element.id}_${i}`).addTo(element, 'now');
    }
  }
  fixPlaceholder(element);
}

function fillMatches(cards, element) {
  if (!ruleset) {
    return;
  }
  const containers = [];
  const values = ['1', ...Object.keys(ruleset.cardValues)];
  for (const group of values) {
    let container = element.querySelector('.group' + group);
    if (!container) {
      container = document.createElement('DIV');
      container.className = 'cardstack group' + group;
      element.appendChild(container);
    }
    containers.push(container);
  }
  const moves = [];
  for (const index of cards) {
    const value = cardGroups[index] || '1';
    const card = HanafudaCard.get(index);
    if (card.isVisible && card.zone != 'mymatches' && card.zone != 'oppmatches') {
      moves.push({ card, value });
    } else {
      card.addTo(containers[values.indexOf(value)]);
    }
  }
  if (moves.length) {
    while (moves.length) {
      const { card, value } = moves.pop();
      card.moveTo(containers[values.indexOf(value)], !moves.length ? 'queue' : 'coalesce');
    }
    animQ.enqueue(() => HanafudaCard.updatePositions(zones[element.id]));
  }
  animQ.enqueue(() => {
    for (const container of Object.values(containers)) {
      let label = container.querySelector('.stacklabel');
      const ct = container.childElementCount - (label ? 1 : 0);
      if (!ct) {
        if (label) {
          label.style.display = 'none';
        }
        continue;
      }
      if (!label) {
        label = document.createElement('DIV');
      }
      label.style.display = '';
      label.className = 'stacklabel';
      label.innerText = ct;
      container.appendChild(label);
    }
  });
}

const deck = HanafudaCard.get('deck', true);
const placeholder = HanafudaCard.get('placeholder', true);

function updateBoard(data, noAnimate) {
  const oldDisableAnimations = disableAnimations;
  if (noAnimate) {
    disableAnimations = true;
  }
  pendingRemove = [...Object.values(HanafudaCards)];
  playerNumber = data.whoami;
  currentPlayer = data.turn;
  currentMonth = data.month;
  userList = data.users;
  gameState = data.mode;
  deck.setLabel(data.deck);
  fillZone(data.field, UI.field);
  fillZone(data.hand, UI.myhand);
  fillZone(data.opponentHand, UI.opphand);
  fillMatches(data.matches, UI.mymatches);
  fillMatches(data.opponentMatches, UI.oppmatches);
  fixPlaceholder();
  let scoreHtml = `Month: ${data.month}`;
  if (ruleset) {
    scoreHtml += ` / ${ruleset.months}`;
  }
  if (ruleset && data.month > ruleset.months) {
    scoreHtml = 'Winner: ';
    if (data.scores[0] == data.scores[1]) {
      scoreHtml += '<b>Draw</b>';
    } else if (data.scores[0] > data.scores[1]) {
      scoreHtml += data.users[0];
    } else {
      scoreHtml += data.users[1];
    }
  } else if (data.month < 1) {
    if (data.users[0]) {
      scoreHtml = 'Waiting for players';
    } else {
      scoreHtml = 'Game aborted';
    }
  }
  function playerLine(i) {
    const indicator = (currentPlayer == i) ? '<span style="color:#ffff00">&#x25cf;</span> ' : '';
    return data.users[i] ? `<br/>${indicator}${data.users[i]}: ${data.scores[i]}` : '<br/>&mdash;: 0';
  }
  const p0line = playerLine(0);
  const p1line = playerLine(1);
  scoreHtml += (playerNumber != 1 ? p1line + p0line : p0line + p1line);
  if (!data.users[0]) {
    scoreHtml += '<br/>Room closed';
  } else if (data.month > ruleset.months && data.users[1] && playerNumber < 2) {
    scoreHtml += '<br/><a class="rematch" onclick="requestRematch()">Rematch</a>';
  } else if (!data.users[1] && playerNumber != 0) {
    scoreHtml += `<br/><a class="rematch" href='${location.pathname}/join'>Join</a>`;
  } else if (!data.users[1] && playerNumber == 0) {
    scoreHtml += `<br/><a class="rematch ${ruleset && ruleset.months == 12 && "selected" || ''}" href='${location.pathname}/ruleset?months=12'>12</a>`;
    scoreHtml += `<a class="rematch ${ruleset && ruleset.months == 6 && "selected" || ''}" href='${location.pathname}/ruleset?months=6'>6</a>`;
    scoreHtml += `<a class="rematch ${ruleset && ruleset.months == 3 && "selected" || ''}" href='${location.pathname}/ruleset?months=3'>3</a>`;
    scoreHtml += `<br/><a class="rematch" href='${location.pathname}/ruleset?vsai=1'>vs. AI</a>`;
  }
  UI.scorezone.innerHTML = scoreHtml;
  ChatUI.playername.innerText = data.users[playerNumber];
  if (faceup) {
    faceup.selected = false;
    faceup = null;
  }
  if (data.faceup !== null) {
    faceup = HanafudaCard.get(data.faceup);
    faceup.addTo(deck.element, 'now');
    faceup.selected = isPlayableState() == 'draw';
  }
  if (data.mode != 'play') {
    clearSelection();
  }
  reportIdle(data.idle.filter(p => p.id < 2));
  if (data.mode == 'koikoi?' || data.mode == 'continue?') {
    animQ.enqueue(() => promptKoikoi(data.prompt));
  } else if (koikoiDlg && koikoiDlg.isVisible) {
    lastKoikoiData = null;
    koikoiDlg.close();
  }
  for (const card of pendingRemove) {
    if (card == deck || card == placeholder) {
      // Special card objects, don't remove them
      continue;
    }
    card.remove();
  }
  pendingDeal = false;
  animQ.enqueue(updateEnabled);
  animQ.enqueue(HanafudaCard.updatePositions);
  disableAnimations = oldDisableAnimations;
}

function matchCardFromHand(card) {
  if (card.selected) {
    card.selected = false;
    clearSelection();
  } else if (card.zone == 'myhand') {
    clearSelection();
    card.selected = true;
    selectedCard = card;
  } else if (selectedCard && card.zone == 'field') {
    gameState = 'network';
    if (card === placeholder) {
      srv.emit('discard', { hand: selectedCard.index });
    } else {
      srv.emit('match', { hand: selectedCard.index, field: card.index });
    }
    clearSelection();
  }
  updateEnabled();
}

function updateEnabled() {
  let p0 = 0, p1 = 0, field = 0;
  const playableState = isPlayableState();
  if (playerNumber > 1) {
    p0 = (0 == currentPlayer) ? 0 : -1;
    p1 = (0 != currentPlayer) ? 0 : -1;
    field = 0;
  } else if (playableState == 'play') {
    p0 = +1;
    p1 = -1;
    field = selectedCard ? 1 : 0;
  } else if (playableState == 'draw') {
    p0 = -1;
    p1 = -1;
    field = 1;
  } else {
    p0 = (playerNumber == currentPlayer) ? 0 : -1;
    p1 = (playerNumber != currentPlayer) ? 0 : -1;
    field = 0;
  }

  UI.myhand.classList.toggle('disabled', p0 == -1);
  UI.myhand.classList.toggle('enabled', p0 == +1);
  UI.opphand.classList.toggle('disabled', p1 == -1);
  UI.opphand.classList.toggle('enabled', p1 == +1);

  if (field != 1) {
    for (const card of zones.field) {
      card.disabled = field == -1;
      card.enabled = false;
    }
    placeholder.disabled = true;
    placeholder.enabled = false;
  } else {
    let discard = true;
    const month = (faceup || selectedCard || { month: -1 }).month;
    for (const card of zones.field) {
      const match = month === card.month;
      discard = discard && !match;
      card.disabled = !match;
      card.enabled = match;
    }
    placeholder.disabled = !discard;
    placeholder.enabled = discard;
  }
  if (placeholder.enabled) {
    placeholder.updatePosition();
  }
}

function matchCardFromDeck(card) {
  gameState = 'network';
  clearSelection();
  if (card === placeholder) {
      srv.emit('draw_table', {});
  } else {
      srv.emit('draw_match', { field: card.index });
  }
  updateEnabled();
}

const animationFns = {
  _ensureSource: function(source, card, target) {
    if (source == 'deck') {
      return Promise.resolve(card.addTo(deck.element, 'now'));
    } else if (card.isVisible) {
      return Promise.resolve();
    } else {
      const zone = zones[(playerNumber > 0) == (source == 0) ? 'opphand' : 'myhand'];
      if (zone.length > 0) {
        zone[zone.length - 1].remove(true);
      }
      return Promise.resolve(card.addTo(UI[zone], 'now'));
    }
  },
  match: function matchAnim(anim) {
    const target = HanafudaCard.get(anim.field);
    const card = HanafudaCard.get(anim.card);
    return animationFns._ensureSource(anim.source, card, target.element)
      .then(() => card.moveTo(target.element, 'now'))
      .then(() => delay(250));
  },
  discard: function discardAnim(anim) {
    const card = HanafudaCard.get(anim.card);
    return animationFns._ensureSource(anim.source, card, placeholder)
      .then(() => {
        placeholder.disabled = true;
        placeholder.enabled = false;
        return card.moveTo(UI.field, 'now');
      })
      .then(() => delay(250));
  }
};

function animateCards(anim) {
  animQ.enqueue(() => animationFns[anim.action] && animationFns[anim.action](anim));
}

async function callKoikoi() {
  await srv.call('koikoi');
  koikoiDlg.close();
}

async function callShobu() {
  await srv.call('shobu');
  koikoiDlg.close();
}

function promptKoikoi(data) {
  const name = userList[data.player] || ('Player ' + (data.player + 1));
  let message = `<p>${name} scored ${data.score} points:</p><ul>`;
  let autoWin = gameState == 'continue?';
  for (const yaku of data.yaku) {
    if (yaku.name == 'teshi' || yaku.name == 'kuttsuki') {
      autoWin = true;
    }
    message += `<li>${yaku.name.replace(/_/g, ' ')} (${yaku.points} point${yaku.points != 1 ? 's' : ''})<br/>`;
    message += yaku.cards.map(card => `<div class='yaku card-${card}' style='background-image:url(${cardImageURL(card)})'></div>`).join('');
    message += '</li>';
  }
  message += '</ul>';
  const divID = 'koikoi_' + (currentMonth || 1) + '_' + data.player + '_' + data.score;
  const div = document.createElement('DIV');
  div.id = divID;
  div.innerHTML = message;

  message += '<br/>';
  let waiting = false;
  if (data.player == playerNumber) {
    message += (autoWin ? '' : '<button name="koikoi">Koi-Koi</button> ') + '<button name="shobu">Shobu</button>';
  } else {
    message += '<i>(Please wait...)</i>';
    waiting = true;
  }

  if (lastKoikoiData !== message) {
    lastKoikoiData = message;
    const existing = document.getElementById(divID);
    if (existing) {
      existing.parentElement.removeChild(existing);
    }
    ChatUI.addLine(div);

    if (koikoiDlg) {
      koikoiDlg.close();
    }
    koikoiDlg = DialogUI.push(message);
    koikoiDlg.koikoi && koikoiDlg.koikoi.addEventListener('click', callKoikoi);
    koikoiDlg.shobu && koikoiDlg.shobu.addEventListener('click', callShobu);
  }

  if (waiting) {
    setTimeout(() => srv.call('board_state').then(data => updateBoard(data)), 1000);
  }
}

function promptDraw() {
  ChatUI.addMessage({ src: 'System', msg: 'The round has ended in a draw.' });
}

function reportIdle(users) {
  if (idleDlg) {
    idleDlg.close();
  }
  const idlePlayers = users.filter(p => p.id < 2);
  if (idlePlayers.length) {
    let message;
    if (idlePlayers.length == 1) {
      message = idlePlayers[0].name + ' appears to have disconnected.';
    } else {
      message = idlePlayers.map(p => p.name).join(' and ') + ' appear to have disconnected.';
    }
    message += '<br/><br/>Please wait...';
    idleDlg = DialogUI.enqueue(message);
  }
}

function promptLeave() {
  // TODO: don't leave when certain important dialogs are open
  const canSpectate = playerNumber < 2 && userList[1] && (currentMonth < 1 || currentMonth > 12);
  const canJoin = playerNumber > 1 && !userList[1] && (currentMonth < 1 || currentMonth > 12);
  DialogUI.push(`
    Really leave the room?<br/>
    <a class='button' href='${location.pathname}/leave'>Yes, leave</a><br/>
    ${canJoin ? "<a class='button' href='" + location.pathname + "/join'>Play Next Game</a><br/>" : ''}
    ${canSpectate ? "<a class='button' href='" + location.pathname + "/spectate'>Spectate</a><br/>" : ''}
    <a class='button cancel'>Cancel</a><br/>
  `);
}

function requestRematch() {
  srv.emit('rematch', {});
}
window.requestRematch = requestRematch;

function roomExpired() {
  // room expired, redirect required
  srv.stop();
  DialogUI.push(`
    This room has expired.<br/>
    <a class='button' href='${urlPrefix}/lobby'>Return to Lobby</a>
  `);
}

function startDeal() {
  for (const el of document.querySelectorAll('.stacklabel')) {
    el.parentElement.removeChild(el);
  }
  pendingDeal = true;
  document.body.classList.remove('animate');
}

UI.game.style.display = '';
deck.addTo(UI.deckzone, 'now');
placeholder.element.classList.add('placeholder');
placeholder.element.innerHTML = 'Discard';
placeholder.disabled = true;
srv.addHandler('update', data => animQ.enqueue(() => updateBoard(data.state)));
srv.addHandler('yaku', data => animQ.enqueue(() => promptKoikoi(data)));
srv.addHandler('round_draw', promptDraw);
srv.addHandler('animate', animateCards);
srv.addHandler('deal', startDeal);
srv.addHandler('idle', data => reportIdle(data.users));
srv.addHandler('lobby', roomExpired);
ChatUI.leave.addEventListener('click', () => promptLeave());
ChatUI.leave.style.visibility = '';
window.addEventListener('resize', () => HanafudaCard.updatePositions());
onLoadCardset(() => srv.call('board_state').then(data => updateBoard(data, true)));

async function startPolling() {
  await ChatUI.init();
  while (!ChatUI.playerName) {
    try {
      await ChatUI.promptUsername(true);
    } catch (err) {
      // If the username was rejected, prompt again.
      console.error(err);
    }
  }
  ruleset = await srv.call('ruleset')
  cardGroups = {};
  for (const group in ruleset.cardValues) {
    for (const card of ruleset.cardValues[group]) {
      cardGroups[card] = group;
    }
  }
  const data = await srv.call('board_state');
  updateBoard(data, true);
}
startPolling();
