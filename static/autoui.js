export default class AutoUI {
  constructor(container) {
    if (container.id) {
      this[container.id] = container;
    }
    for (const el of container.querySelectorAll('[id],[name]')) {
      if (el.name && !(el.name in this)) {
        this[el.name] = el;
      } else if (el.id && !(el.id in this)) {
        this[el.id] = el;
      }
    }
  }
}

const dlgStack = [];

function makeStandardButton(label) {
  const button = document.createElement('BUTTON');
  button.innerText = label;
  button.id = 'std_' + label.replace(/\W/g, '');
  if (label == 'Cancel') {
    button.className = 'cancel';
  }
  return button;
}

class AutoDialog extends AutoUI {
  constructor(content, buttons = []) {
    const root = document.createElement('DIV');
    if (typeof content == 'string') {
      root.innerHTML = content;
    } else if (Array.isArray(content)) {
      for (const element of content) {
        root.appendChild(element);
      }
    } else {
      root = content;
    }
    const btnRefs = [];
    if (buttons && buttons.length) {
      const buttonArea = document.createElement('DIV');
      buttonArea.style.textAlign = 'center';
      buttonArea.style.marginTop = '1em';
      let first = true;
      for (const button of buttons) {
        if (first) {
          first = false;
        } else {
          buttonArea.appendChild(document.createTextNode(' '));
        }
        const btn = makeStandardButton(button);
        buttonArea.appendChild(btn);
        btnRefs.push(btn);
      }
      root.appendChild(buttonArea);
    }

    super(root);

    this.root = root;
    this.isVisible = false;
    this.promises = [];
    this.close = this.close.bind(this);
    this.submit = this.submit.bind(this);
    this.cancel = this.cancel.bind(this);
    this._onError = this._onError.bind(this);

    const cancelButton = this.root.querySelector('.cancel');
    if (cancelButton) {
      cancelButton.addEventListener('click', this.cancel);
      cancelButton.addEventListener('keydown', event => (event.key == "Enter" || event.key == "Return" || event.key == " " || event.key == "Spacebar") && this.cancel(event));
      this.root.addEventListener('keydown', event => (event.key == "Escape" || event.key == "Esc") && this.cancel(event));
    }

    const formElement = this.root.querySelector('form');
    if (formElement) {
      formElement.addEventListener('submit', this.submit);
    }

    for (const btn of btnRefs) {
      if (!btn.classList.contains('cancel')) {
        btn.addEventListener('click', this.submit);
      }
    }
  }

  show() {
    if (!DialogUI.dlgshim.classList.contains('dlghidden')) {
      // A dialog is already visible
      return;
    }

    DialogUI.dlgshim.classList.remove('dlghidden');
    DialogUI.dialog.innerHTML = '';
    DialogUI.dialog.appendChild(this.root);
    this.isVisible = true;

    let tabIndex = 1;
    for (const element of this.root.querySelectorAll('input, select, button, a')) {
      element.tabIndex = tabIndex;
      if (tabIndex == 1) {
        element.focus();
      }
      ++tabIndex;
    }
  }

  hide() {
    DialogUI.dlgshim.classList.add('dlghidden');
    this.isVisible = false;
  }

  close() {
    const index = dlgStack.indexOf(this);
    if (index >= 0) {
      dlgStack.splice(index, 1);
    }
    if (this.isVisible) {
      this.hide();
      DialogUI.dialog.innerHTML = '';
      const nextDlg = dlgStack.pop();
      if (nextDlg) {
        nextDlg.show();
      }
    }
    this.onClose(this);
    for (const promise of this.promises) {
      promise.resolve(this);
    }
    this.promises = [];
  }

  submit(event) {
    Promise.resolve(this.onSubmit(this))
      .then(this.close)
      .catch(this._onError);
    if (event) {
      event.preventDefault();
      return false;
    }
  }

  cancel(event) {
    Promise.resolve(this.onCancel(this))
      .then(this.close)
      .catch(this._onError);
    if (event) {
      event.preventDefault();
      return false;
    }
  }

  onClose(dlg) {}
  onSubmit(dlg) {}
  onCancel(dlg) {}
  onError(dlg, err) {}

  _onError(err) {
    if (!err) {
      // Quiet cancel
      return;
    }
    console.error(err);
    for (const promise of this.promises) {
      promise.reject(err);
    }
    this.onError(this, err);
  }

  promise() {
    return new Promise((resolve, reject) => this.promises.push({ resolve, reject }));
  }
}

class DialogUIImpl extends AutoUI {
  constructor() {
    super(document.getElementById('dlgshim'));
    this.dlgshim.addEventListener('mousedown', this.fade.bind(this));
    this.dlgshim.addEventListener('touchstart', this.fade.bind(this));
    this.dlgshim.addEventListener('mouseup', this.unfade.bind(this));
    this.dlgshim.addEventListener('touchend', this.unfade.bind(this));
  }

  fade(event) {
    if (event.target == this.dlgshim) {
      event.preventDefault();
      this.dlgshim.classList.add('dlgfade');
      return false;
    }
  }

  unfade(event) {
    if (this.dlgshim.classList.contains('dlgfade')) {
      event.preventDefault();
      this.dlgshim.classList.remove('dlgfade');
      return false;
    }
  }

  get isVisible() {
    return !this.dlgshim.classList.contains('dlghidden');
  }

  push(content, standardButtons = []) {
    const dlg = new AutoDialog(content, standardButtons);
    if (dlgStack.length) {
      dlgStack[dlgStack.length - 1].hide();
    }
    dlgStack.push(dlg);
    dlg.show();
    return dlg;
  }

  enqueue(content, standardButtons = []) {
    const dlg = new AutoDialog(content, standardButtons);
    dlgStack.unshift(dlg);
    if (!this.isVisible) {
      dlg.show();
    }
    return dlg;
  }
}

export const DialogUI = new DialogUIImpl();
