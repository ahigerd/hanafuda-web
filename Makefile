build: .deps-installed

Pipfile.lock: Pipfile
	pipenv lock

.deps-installed: Pipfile Pipfile.lock
	pipenv sync
	touch .deps-installed

run: .deps-installed
	pipenv run uwsgi hanafuda.ini

uwsgi: run
