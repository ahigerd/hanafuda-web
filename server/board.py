import random
from .card import Cards
from .yaku import HanafudaYaku

class BoardState:
    def __init__(self, ruleset, dealer = None):
        self.ruleset = ruleset
        self.deck = list(Cards)
        self.field = []
        self.hands = [[], []]
        self.matches = [[], []]
        self.hand_scores = [0, 0]
        self.faceup = None
        self.auto_score = None
        self.auto_yaku = None
        if dealer is None:
            self.dealer = 0
            self.state = ('game_start', -1)
        else:
            self.dealer = dealer
            self.state = ('round_start', -1)

    def _assert_state(self, state, player=-1):
        if isinstance(state, str):
            state = [state]
        if self.state[0] not in state:
            raise Exception('expected state "%s", got "%s"' % ('" or "'.join(state), self.state[0]))
        if player >= 0 and self.state[1] != player:
            raise Exception('wrong player')

    def _draw(self):
        if not len(self.deck):
            return None
        i = random.randint(0, len(self.deck) - 1)
        return self.deck.pop(i)

    def determine_first(self):
        self._assert_state(['game_start', 'finish'])
        # TODO: animate
        self.dealer = None
        self.deck = list(Cards)
        while self.dealer is None:
            if len(self.deck) < 2:
                self.deck = list(Cards)
            p1 = self._draw()
            p2 = self._draw()
            if p1.month < p2.month:
                self.dealer = 0
            elif p1.month > p2.month:
                self.dealer = 1
        self.state = ('round_start', -1)
        return self.dealer

    def deal(self, test=False):
        self._assert_state('round_start')
        self.deck = list(Cards)
        # TODO: animate
        if test:
            player, mode = test
            if mode == 'void':
                self.field = self.deck[0:4]
                self.deck = self.deck[4:]
            elif mode == 'teshi':
                self.hands[player] = self.deck[0:4]
                self.deck = self.deck[4:]
            elif mode == 'kuttsuki':
                for i in [14, 13, 10, 9, 5, 4, 1, 0]:
                    self.hands[player].append(self.deck.pop(i))
            elif mode == 'draw':
                self.hands[player] = self.deck[0:4]
                self.hands[1 - player] = self.deck[4:8]
                self.deck = self.deck[8:]
        for i in range(4):
            if len(self.hands[1 - self.dealer]) < 8:
                self.hands[1 - self.dealer].append(self._draw())
                self.hands[1 - self.dealer].append(self._draw())
            if len(self.field) < 8:
                self.field.append(self._draw())
                self.field.append(self._draw())
            if len(self.hands[self.dealer]) < 8:
                self.hands[self.dealer].append(self._draw())
                self.hands[self.dealer].append(self._draw())
        self.state = ('play', self.dealer)

    def _find_by_month(self, card):
        if not card:
            return []
        month_cards = []
        for c in self.field:
            if c.month == card.month:
                month_cards.append(c)
        return month_cards

    def discard(self, player, hand):
        self._assert_state('play', player)
        player_card = None
        for c in self.hands[player]:
            if c.index == hand:
                player_card = c
                break
        month_cards = self._find_by_month(player_card)
        if not player_card or len(month_cards):
            raise Exception('illegal move: discarding %s' % hand)
        self.hands[player].remove(player_card)
        self.field.append(player_card)
        self.faceup = self._draw()
        self.state = ('draw', player)

    def match(self, player, hand, field):
        if self.faceup:
            self._assert_state('draw', player)
        else:
            self._assert_state('play', player)
        player_card = None
        field_card = None
        if self.faceup and self.faceup.index == hand:
            player_card = self.faceup
        elif not self.faceup:
            for c in self.hands[player]:
                if c.index == hand:
                    player_card = c
                    break
        month_cards = self._find_by_month(player_card)
        for c in month_cards:
            if c.index == field:
                field_card = c
        if not player_card or not field_card:
            raise Exception('illegal move: playing %s on %s' % (hand, field))
        self.matches[player].append(player_card)
        if len(month_cards) == 3:
            for c in month_cards:
                self.field.remove(c)
                self.matches[player].append(c)
        else:
            self.field.remove(field_card)
            self.matches[player].append(field_card)
        if self.faceup:
            self._end_turn()
        else:
            self.hands[player].remove(player_card)
            self.faceup = self._draw()
            self.state = ('draw', self.state[1])

    def draw_table(self, player):
        self._assert_state('draw', player)
        month_cards = self._find_by_month(self.faceup)
        if len(month_cards):
            raise Exception('illegal move: tabling %s' % self.faceup)
        self.field.append(self.faceup)
        self._end_turn()

    def _end_turn(self):
        self.faceup = None
        p = self.state[1]
        score, _ = HanafudaYaku(self.matches[p]).compute_score(False)
        if score != self.hand_scores[p]:
            self.hand_scores[p] = score
            self.state = ('koikoi?', p)
        else:
            self.state = ('play', 1 - p)

    def koikoi(self, player):
        self._assert_state('koikoi?', player)
        self.state = ('play', 1 - self.state[1])

    def shobu(self, player):
        self._assert_state(['koikoi?', 'continue?'], player)
        self.state = ('play', player)

    def auto_win(self, player, score=None, yaku=None):
        self.auto_score = score
        self.auto_yaku = yaku
        self.state = ('continue?', player)

    def void_round(self, switch_players=False):
        self._assert_state(['play'])
        if switch_players:
            self.state = ('round_start', 1 - self.dealer)
        else:
            self.state = ('round_start', self.dealer)
        self.deck = list(Cards)
        self.field = []
        self.hands = [[], []]
        self.matches = [[], []]

    def check_initial(self):
        hands = [self.field, self.hands[0], self.hands[1]]
        results = []
        for p, cards in enumerate(hands):
            teshi, teshi_cards = HanafudaYaku.teshi(cards)
            kuttsuki, kuttsuki_cards = HanafudaYaku.kuttsuki(cards)
            for name in ('teshi', 'kuttsuki'):
                points, yaku_cards = getattr(HanafudaYaku, name)(cards)
                if points:
                    result = (p - 1, [{
                        'name': name,
                        'points': points,
                        'cards': [c.index for c in yaku_cards],
                    }])
                    if p == 0:
                        return result
                    results.append(result)
                    break
        if len(results) == 0:
            return False, []
        elif len(results) == 1:
            return results[0]
        else:
            return 'draw', [r[1][0] for r in results]

    def end_game(self, player=None):
        self.state = ('finish', -1)
