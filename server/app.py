import werkzeug.exceptions as wz_exceptions
from http.cookies import SimpleCookie
from .content import register_content, RedirectException
from .fdpool import fdpool
import json
import flask
import os
import sys
import uwsgi
import json
import time
import traceback
import selectors

if 'url-prefix' in uwsgi.opt:
    PREFIX = uwsgi.opt['url-prefix']
else:
    PREFIX = os.getenv('URL_PREFIX', '/hanafuda')

if isinstance(PREFIX, bytes):
    PREFIX = PREFIX.decode()

class WsInstance:
    def __init__(self, playerID, view, kwargs):
        self.playerID = playerID
        self.view = view
        self.view_kwargs = kwargs or {}
        self._on_connect = getattr(view, 'on_connect', None)
        self._on_disconnect = getattr(view, 'on_disconnect', None)
        self.pending = []
        self.ping_expiration = None

        self._fd = uwsgi.connection_fd()
        self._pipe = fdpool.acquire()

        from . import sessions
        self.session = sessions.set_context_session(id=self.playerID)
        self.session.sockets.append(self)
        sessions.set_context_session(id=None, redirect=False)

    def __del__(self):
        self.dispose()

    def dispose(self):
        from .sessions import expire_sessions
        if self._pipe:
            self._pipe.dispose()
            self._pipe = None
        if callable(self._on_disconnect):
            self._on_disconnect(self, **self.view_kwargs)
            self._on_disconnect = None
        if self.session:
            self.session.sockets.remove(self)
            self.session = None
            expire_sessions()

    def run(self):
        try:
            if callable(self._on_connect):
                self._on_connect(self, **self.view_kwargs)
            self.is_closing = False
            while not self.is_closing:
                self.handle_one()
        finally:
            self.dispose()

    def handle_one(self):
        _id, result = self.next_message()
        if not result:
            return
        if type(result) == tuple or type(result) == list:
            result = result[0]
        if type(result) != dict:
            try:
                result = json.loads(result)
            except:
                pass
            if type(result) != dict:
                result = { "result": result }
        if _id is not None:
            result['_id'] = _id
        if 'timestamp' not in result:
            result['timestamp'] = time.monotonic()
        result = json.dumps(result).encode()
        try:
            uwsgi.websocket_send(result)
        except IOError:
            self.is_closing = True

    def emit(self, msg):
        self.pending.append(msg)
        self._pipe.signal()

    def next_message(self):
        if self._pipe is None or self._pipe.fd is None:
            self.is_closing = True
            return None, None
        msg = None
        sel = selectors.DefaultSelector()
        sel.register(self._fd, selectors.EVENT_READ)
        sel.register(self._pipe.fd, selectors.EVENT_READ)
        try:
            ready = sel.select(30)
            if not ready:
                if self.ping_expiration and self.ping_expiration < time.monotonic():
                    self.is_closing = True
                    return None, None
                self.ping_expiration = time.monotonic() + 60
                return None, { 'eventName': 'ping' }
            for key, evts in ready:
                if key.fd == self._fd:
                    msg = json.loads(uwsgi.websocket_recv_nb())
                elif key.fd == self._pipe.fd:
                    events = []
                    for i in range(self._pipe.num_signals()):
                        events.append(self.pending.pop(0))
                    for event in events:
                        uwsgi.websocket_send(json.dumps(event).encode())
        except IOError:
            self.is_closing = True
            return None, None
        if msg:
            return self.dispatch(msg)
        return None, None

    def dispatch(self, msg):
        from . import sessions
        _id = None
        if self.is_closing or not self.session:
            return None, None
        try:
            _id = msg.pop('_id', None)
            func = msg.pop('_func')
            sessions.set_context_session(id=self.playerID)
            if func == 'pong':
                self.ping_expiration = None
                self.session.ping()
                return None, None
            return _id, self.view_function(func, msg)
        except RedirectException:
            self.is_closing = True
            return None, None
        except Exception as e:
            sys.stderr.write('dispatch %s: %s\n' % (type(e).__name__, str(e)))
            traceback.print_exc()
            return _id, { "error": '%s: %s' % (type(e).__name__, str(e)) }
        finally:
            sessions.set_context_session(id=None, redirect=False)

    def view_function(self, fn, msg):
        return self.view(**msg, **self.view_kwargs, fn=fn)

class WsApp:
    def __init__(self, app):
        self.app = app

    def __call__(self, env, start_response):
        if 'HTTP_SEC_WEBSOCKET_KEY' not in env:
            return self.app(env, start_response)
        try:
            inst = self.resolve(env)
        except wz_exceptions.HTTPException as e:
            return e(env, start_response)
        except:
            return wz_exceptions.NotFound(env, start_response)
        inst.run()

    def resolve(self, env):
        endpoint, kwargs = self.app.url_map.bind_to_environ(env).match()
        cookies = SimpleCookie()
        try:
            cookies.load(env['HTTP_COOKIE'])
            playerID = cookies['playerID'].value
        except:
            raise wz_exceptions.Unauthorized()
        uwsgi.websocket_handshake(env['HTTP_SEC_WEBSOCKET_KEY'], env.get('HTTP_ORIGIN', ''))
        view = self.app.view_functions[endpoint]
        return WsInstance(playerID, view, kwargs)

    def on_connect(self, callback):
        def decorate(fn):
            fn.on_connect = callback
            return fn
        return decorate

    def on_disconnect(self, callback):
        def decorate(fn):
            fn.on_disconnect = callback
            return fn
        return decorate

app = flask.Flask(__name__)
app.config['TESTING'] = False
app.config['DEBUG'] = False
register_content(app, PREFIX)
wsapp = WsApp(app)

@app.errorhandler(RedirectException)
def do_redirect(exc):
    return flask.redirect(PREFIX + exc.url, code=exc.status_code)
