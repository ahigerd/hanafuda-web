from .app import app
import os

app.run(host=os.getenv('HOST', '0.0.0.0'), port=os.getenv('PORT', 5000), threaded=True)
