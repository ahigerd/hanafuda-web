import os

class FdWait(dict):
    def __init__(self, fdpair, pool):
        super(FdWait, self).__init__()
        self._r, self._w = fdpair
        os.set_blocking(self._r, False)
        self._pool = pool
        self._disposed = False

    def __del__(self):
        self.dispose()

    @property
    def fd(self):
        if self._disposed:
            return None
        return self._r

    def dispose(self):
        if not self._disposed:
            self._disposed = True
            self._pool.release(self)

    def num_signals(self):
        if self._disposed: return 0
        count = 0
        try:
            while True:
                os.read(self._r, 1)
                count += 1
        except BlockingIOError:
            return count

    def signal(self):
        if self._disposed: return
        os.write(self._w, b'.')


class FdPool:
    def __init__(self):
        self._fds = []

    def acquire(self):
        if not len(self._fds):
            return FdWait(os.pipe(), self)
        fdwait = self._fds.pop(0)
        fdwait._disposed = False
        return fdwait

    def release(self, fdwait):
        self._fds.append(fdwait)


fdpool = FdPool()

