from .card import Cards
from .content import RedirectException
import time
import threading
import queue
import random
import traceback
import pprint

class AIPlayer:
    def __init__(self, room):
        self.room = room
        self.did_koikoi = False
        self.last_state = None
        self.queue = queue.Queue()
        self.thread = threading.Thread(target=self.run, name='AI-%s' % room.id, daemon=True)
        self.thread.start()

    def update(self, state):
        self.queue.put(state)

    def command(self, player, message):
        self.queue.put({ 'mode': 'command', 'command': message, 'source': player })

    def dispose(self):
        self.queue.put(None)

    def run(self):
        try:
            while self.process_one():
                pass
        except Exception as e:
            state = self.room.board_state('ai')
            self.room.logfile.write('AI error\n')
            traceback.print_exc()
            pprint.pprint(state, compact=True, width=120)
            traceback.print_exc(file=self.room.logfile)
            pprint.pprint(state, stream=self.room.logfile, compact=True, width=120)
            self.room.message(
                player='ai',
                msg='Sorry, I did something I shouldn\'t have. Please include room #%s in a bug report.' % self.room.id,
            )
            self.room._ruleset['vsAI'] = False
            try:
                self.room.leave('ai')
            except RedirectException:
                pass

    def legal_moves(self, hand, field):
        moves = []
        for card in hand:
            for other in field:
                if card.month == other.month:
                    moves.append((card, other))
        return self.sort_moves(moves)

    def sort_moves(self, moves):
        return sorted(moves, key=lambda m: self.card_value(m[0]) + self.card_value(m[1]), reverse=True)

    def card_value(self, card):
        return card.value

    def process_one(self):
        state = self.queue.get()
        if not state:
            return False
        if state['mode'] == 'command':
            self.on_command(state['source'], state['command'])
            return True

        self.me = state['whoami']
        self.you = 1 - state['whoami']

        new_state = (state['mode'], state['turn'])
        if new_state == self.last_state:
            # If somehow we end up processing the same state twice,
            # don't act on it the second time.
            return True
        self.last_state = new_state

        if self.me != state['turn']:
            return True
        if not len(state['matches']):
            self.did_koikoi = False
        hand = [Cards[i] for i in state['hand']]
        field = [Cards[i] for i in state['field']]
        faceup = state.get('faceup')
        if faceup is not None:
            faceup = Cards[faceup]
        time.sleep(2)
        fn = getattr(self, 'on_' + state['mode'].replace('?', ''), None)
        if fn:
            fn(state, hand, field, faceup)
        return True

    def on_play(self, state, hand, field, faceup):
        moves = self.legal_moves(hand, field)
        if len(moves) > 0:
            self.room.match('ai', moves[0][0].index, moves[0][1].index)
            return
        bymonth = [[]] * 12
        hand.sort(key=lambda c: c.value)
        for card in hand:
            bymonth[card.month].append(card)
        bymonth = sorted((c for c in bymonth if len(c) > 1), key=lambda m: m[-1].value, reverse=True)
        if len(bymonth):
            self.room.discard('ai', bymonth[0][0].index)
        else:
            self.room.discard('ai', hand[0].index)

    def on_draw(self, state, hand, field, faceup):
        moves = self.legal_moves([faceup], field)
        if len(moves) > 0:
            self.room.draw_match('ai', moves[0][1].index)
        else:
            self.room.draw_table('ai')

    def on_koikoi(self, state, hand, field, faceup):
        if self.did_koikoi:
            self.room.shobu('ai')
        elif state['month'] == self.room._ruleset['months'] and state['scores'][self.me] <= state['scores'][self.you]:
            self.did_koikoi = True
            self.room.koikoi('ai')
        elif state['prompt']['score'] > 7 or state['scores'][self.me] + state['prompt']['score'] > state['scores'][self.you]:
            self.room.shobu('ai')
        elif len(state['hand']) > 2 and random.random() < .1 + .1 * len(state['hand']):
            self.did_koikoi = True
            self.room.koikoi('ai')
        else:
            self.room.shobu('ai')

    def on_continue(self, state, hand, field, faceup):
        self.room.shobu('ai')

    def on_command(self, source, command):
        print('AI command from %s: %s' % (source, command))
        self.room.logfile.write('[%s] AI command from %s: %s\n' % (self.room.id, source, command))
        if command == 'crash' and source in self.room.players:
            raise Exception('crash requested')
