from .content import json_response
from . import sessions
import flask
import json

app = flask.Blueprint('status', __name__, static_folder='../static')

@app.route('/')
def status_index():
    return json_response({ 'ok': 1 })

@app.route('/sessions')
def sessions_status():
    from . import sessions
    sessions.expire_sessions()
    html = []
    for session_id, session in sessions.sessions.items():
        html.append('%s (x%d): %s' % (
            session.name,
            len(session.sockets),
            ', '.join(map(lambda r: '%s (%s)' % (r, len(session.rooms[r])), session.rooms.keys()))
        ))
    return '<ul>%s</ul>' % ''.join(map(lambda r: '<li>%s</li>' % (r,), html))
