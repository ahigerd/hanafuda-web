from .app import PREFIX, app as _app
from .room import app as room_app
from .lobby import app as lobby_app
from .settings import app as settings_app
from .status import app as status_app
import flask
import os
import sys
import time

_app.register_blueprint(lobby_app, url_prefix=PREFIX + '/lobby')
_app.register_blueprint(room_app, url_prefix=PREFIX + '/room')
_app.register_blueprint(settings_app, url_prefix=PREFIX + '/settings')
_app.register_blueprint(status_app, url_prefix=PREFIX + '/status')

@_app.route(PREFIX)
@_app.route(PREFIX + '/')
@_app.route('/')
def redirect_to_lobby():
    # TODO: redirect to current room
    return flask.redirect(PREFIX + '/lobby/')

@_app.after_request
def disable_keepalive(response):
    for cookie, value in flask.request.cookies.items():
        response.set_cookie(cookie, value, path=PREFIX, httponly=False, samesite='strict', domain=flask.request.headers['host'])
    response.headers.set('Connection', 'close')
    response.headers.remove('Keep-Alive')
    return response
