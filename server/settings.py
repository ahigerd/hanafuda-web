from .content import static_path, ContentCache, JsonCache, json_response
from glob import glob
import flask
import json
import os

app = flask.Blueprint('settings', __name__, static_folder='../static')

json_cache = JsonCache()
def get_json_files(infix):
    files = {}
    for fn in glob(static_path('*.%s.json' % infix)):
        key = os.path.basename(fn)
        files[key] = json_cache.get(key)
    return files

def get_themes():
    result = []
    for key, theme in get_json_files('theme').items():
        key = key.split('.')[0]
        result.append((key, theme.get('name', key)))
    result.sort(key=lambda x: x[1])
    return dict(result)

def get_sets():
    result = []
    for key, theme in get_json_files('set').items():
        key = key.split('.')[0]
        result.append((key, theme.get('name', key)))
    result.sort(key=lambda x: x[1])
    return dict(result)

@app.route('/json')
def settings_json():
    return json_response({
        "themes": get_themes(),
        "sets": get_sets(),
    })

html_cache = ContentCache()

@app.route('/')
def settings_html():
    html = html_cache.get('settings.html').decode()
    return flask.render_template_string(html, themes=get_themes(), cardsets=get_sets())
