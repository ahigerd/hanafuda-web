from . import card

ALL_YAKU = [
    'gokou',
    'shikou',
    'sankou',
    'ame_shikou',
    'ino_shika_chou',
    'tane',
    'choufuku',
    'akatan',
    'aotan',
    'tanzaku',
    'tsukimi_zake',
    'hanami_zake',
    'kasu',
]

def filter_type(cards, attr):
    return list(c for c in cards if getattr(c, attr))

class HanafudaYaku:
    def __init__(self, cards):
        self.cards = cards
        self._brights = filter_type(cards, 'bright')
        self._animals = filter_type(cards, 'animal')
        self._blue_tanzaku = filter_type(cards, 'blue_tanzaku')
        self._red_tanzaku = filter_type(cards, 'red_tanzaku')
        self._tanzaku = filter_type(cards, 'tanzaku')
        self._chaff = filter_type(cards, 'chaff')

    @staticmethod
    def teshi(hand):
        for suit in range(12):
            matched = []
            for index in range(suit * 4, suit * 4 + 4):
                if card.Cards[index] in hand:
                    matched.append(card.Cards[index])
            if len(matched) == 4:
                return 6, matched
        return False, []

    @staticmethod
    def kuttsuki(hand):
        found = [[] for i in range(12)]
        for card in hand:
            found[card.month].append(card)
        pairs = []
        for cards in found:
            count = len(cards)
            if count == 0:
                continue
            elif count % 2 == 0:
                pairs += cards
            else:
                return False, []
        if len(pairs) == 8:
            return 6, pairs
        return False, []

    @property
    def gokou(self):
        if len(self._brights) == 5:
            return 10, self._brights
        return False, []

    @property
    def shikou(self):
        if len(self._brights) == 4 and card.RAIN not in self._brights:
            return 8, self._brights
        return False, []

    @property
    def sankou(self):
        if len(self._brights) == 3 and card.RAIN not in self._brights:
            return 5, self._brights
        return False, []

    @property
    def ame_shikou(self):
        if len(self._brights) == 4 and card.RAIN in self._brights:
            return 7, self._brights
        return False, []

    @property
    def ino_shika_chou(self):
        if card.BOAR in self.cards and card.DEER in self.cards and card.BUTTERFLY in self.cards:
            return 2 + len(self._animals), self._animals
        return False, []

    @property
    def tane(self):
        if len(self._animals) >= 5 and not self.ino_shika_chou[0]:
            return len(self._animals) - 4, self._animals
        return False, []

    @property
    def choufuku(self):
        if len(self._red_tanzaku) == 3 and len(self._blue_tanzaku) == 3:
            return len(self._tanzaku) + 4, self._tanzaku
        return False, []

    @property
    def akatan(self):
        if len(self._red_tanzaku) == 3 and len(self._blue_tanzaku) < 3:
            return len(self._tanzaku) + 2, self._tanzaku
        return False, []

    @property
    def aotan(self):
        if len(self._red_tanzaku) < 3 and len(self._blue_tanzaku) == 3:
            return len(self._tanzaku) + 2, self._tanzaku
        return False, []

    @property
    def tanzaku(self):
        if len(self._tanzaku) >= 5 and len(self._red_tanzaku) < 3 and len(self._blue_tanzaku) < 3:
            return len(self._tanzaku) - 4, self._tanzaku
        return False, []

    @property
    def tsukimi_zake(self):
        if card.MOON in self.cards and card.CUP in self.cards:
            return 5, [card.MOON, card.CUP]
        return False, []

    @property
    def hanami_zake(self):
        if card.CURTAIN in self.cards and card.CUP in self.cards:
            return 5, [card.CURTAIN, card.CUP]
        return False, []

    @property
    def kasu(self):
        chaff = len(self._chaff)
        cards = self._chaff
        if chaff >= 10:
            return chaff - 9, cards
        return False, []

    def compute_score(self, aite_koi_koi):
        score = 0
        yaku = []
        for name in ALL_YAKU:
            points, cards = getattr(self, name)
            if points:
                score += points
                yaku.append({ 'name': name, 'cards': cards, 'points': points })
        if score >= 7:
            yaku.append({ 'name': 'score', 'cards': [], 'points': 'x2' })
            score *= 2
        if aite_koi_koi:
            yaku.append({ 'name': 'koi_koi', 'cards': [], 'points': 'x2' })
            score *= 2
        return score, yaku
