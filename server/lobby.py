from . import events
from .content import index_html, json_response
from .room import rooms
from .sessions import sessions, current_session
from .app import wsapp
import flask
import time
import copy

app = flask.Blueprint('lobby', __name__, static_folder='../static')
event_group = events.EventGroup('lobby')

def room_dict(room, now=None):
    if not now:
        now = time.monotonic()
    expired = room._is_expired(now)
    if expired or not len(room.players) or room.private:
        return { 'id': room.id, 'hide': True, 'expired': expired }
    return {
        'id': room.id,
        'players': [room._get_name(id) for id in room.players],
        'spectators': [room._get_name(id) for id in room.spectators],
        'scores': [sum(n) for n in room.scores],
        'month': room._month,
        'ruleset': room._ruleset,
        'inRoom': room._users,
    }

def update_room(room):
    event_group.dispatch_event('update', room=room_dict(room), dedupe='room_%s' % room.id)

def set_name(name = None):
    old_name = current_session().name
    new_name = flask.request.args['name'] if name is None else name
    if old_name != new_name:
        did_rename = not old_name.startswith('anonymous#')
        current_session().name = new_name
        for room_id in current_session().rooms:
            if room_id in rooms:
                room = rooms[room_id]
                update_room(room)
                room.dispatch_event('update')
                if did_rename:
                    room.dispatch_event('renamed', name=old_name, newName=new_name)
        if did_rename:
            event_group.dispatch_event('renamed', name=old_name, newName=new_name)
    return { 'ok': True }

def push_message(msg = None):
    if msg is None:
        msg = flask.request.args['msg']
    player = current_session()
    event_group.dispatch_event('message', src=player.name, msg=msg)
    return { "ok": True }

def get_room_list():
    player = current_session(False)
    result = []
    expired = []
    now = time.monotonic()
    for room_id, room in rooms.items():
        data = room_dict(room, now)
        if data.get('expired'):
            expired.append(room_id)
        if not data.get('hide'):
            result.append(data)
    for room_id in expired:
        room = rooms[room_id]
        for player_id in room._users:
            player = sessions.get(player_id, None)
            if not player:
                continue
            if room_id in player.rooms:
                for socket in player.rooms[room_id]:
                    if socket: socket.dispose()
                del player.rooms[room_id]
        room._dispose()
        del rooms[room_id]
    return result

@app.route('/list')
def list_rooms():
    return json_response(get_room_list())

@app.route('/')
def index():
    # ensure that a session exists
    current_session(redirect=False)
    return index_html(('lobby.js',))

ws_funcs = {
    "message": push_message,
    "list": lambda: { 'rooms': get_room_list() },
    "set_name": set_name,
}

def on_ws_disconnect(ws):
    ws.session.leave('lobby', ws)

def on_ws_connect(ws):
    ws.session.join('lobby', ws)

@app.route('/', websocket=True)
@wsapp.on_connect(on_ws_connect)
@wsapp.on_disconnect(on_ws_disconnect)
def dispatch_ws(fn, **kwargs):
    func = ws_funcs.get(fn)
    if not func:
        raise Exception('invalid function')
    return func(**kwargs)
