from .app import app, RedirectException
import flask
import time
import threading

IDLE_TIME = 45
SESSION_TIME = 600

sessions = {}
next_player_id = 1

class UserSession:
    def __init__(self, cookie):
        global next_player_id
        self.cookie = cookie
        self.index = next_player_id
        self.rooms = {}
        self.sockets = []
        self._name = None
        next_player_id += 1
        self.ping()

    def __del__(self):
        self.dispose()

    def dispose(self):
        for room_id, room in self.rooms.items():
            for sock in room:
                self.leave(room_id, sock)
        for sock in self.sockets:
            sock.session = None
            sock.dispose()
        self.sockets = []

    def join(self, room, socket):
        if room not in self.rooms:
            self.rooms[room] = []
        if socket and socket not in self.rooms[room]:
            self.rooms[room].append(socket)

    def leave(self, room, socket):
        if room is None:
            for r in self.rooms:
                if socket is None or socket in self.rooms[r]:
                    self.rooms[r].remove(socket)
                if not self.rooms[r]:
                    del self.rooms[r]
            return
        try:
            self.rooms[room].remove(socket)
        except (KeyError, ValueError):
            pass

    def ping(self):
        self.last_active = time.monotonic()

    def emit(self, event, room=None):
        sockets = self.sockets if room is None else self.rooms.get(room, [])
        for socket in sockets:
            if not socket: continue
            try:
                socket.emit(event)
            except Exception as e:
                import traceback, sys
                sys.stderr.write('emit %s: %s\n' % (type(e).__name__, str(e)))
                traceback.print_exc()

    @property
    def name(self):
        if self._name:
            return self._name
        return 'anonymous#%d' % self.index

    @name.setter
    def name(self, value):
        if value:
            new_name = '%s' % value
        else:
            new_name = None
        if new_name == self._name:
            return
        expire_sessions()
        for other in sessions.values():
            if other._name == new_name:
                raise ValueError('name in use')
        self._name = new_name

class SessionContext(threading.local):
    id = None

ctx_local = SessionContext()
def set_context_session(id, redirect=True):
    ctx_local.id = id
    return current_session(redirect)

def is_websocket_context():
    return ctx_local.id is not None

def current_session(redirect=True, id=None):
    global next_player_id
    if id:
        player = id
    else:
        try:
            player = flask.request.cookies.get('playerID')
        except:
            player = ctx_local.id
    if not player:
        if redirect:
            raise RedirectException('/lobby')
        return None
    if player not in sessions:
        sessions[player] = UserSession(player)
    session = sessions[player]
    session.ping()
    return session

@app.before_request
def expire_sessions():
    from .room import rooms
    # before anything else, update the current session
    current_session(redirect=False)

    now = time.monotonic()
    idle = now - IDLE_TIME
    expires = now - SESSION_TIME
    expired = []
    for id, session in sessions.items():
        if session.last_active < idle:
            for room_id in session.rooms:
                if room_id in rooms:
                    rooms[room_id]._idle(id)
            if session.last_active < expires:
                expired.append(id)
    for id in expired:
        s = sessions[id]
        del sessions[id]
        s.dispose()
