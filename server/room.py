from . import events, card as cards
from .content import index_html, json_response, RedirectException
from .board import BoardState
from .yaku import HanafudaYaku
from .sessions import sessions, current_session, is_websocket_context
from .aiplayer import AIPlayer
from .app import wsapp
import flask
import json
import traceback
import time
import random
import copy

app = flask.Blueprint('room', __name__, static_folder='../static')
rooms = {}

DEFAULT_RULESET = {
    'cardValues': {
        20: cards._brights,
        10: cards._animals,
        5: cards._tanzaku,
        'wild': [cards.CUP.index],
    },
    'months': 12,
    'multiKoikoi': True,
    'vsAI': False,
}

class Room(events.EventGroup):
    def __init__(self, id):
        super(Room, self).__init__(id)
        self.logfile = open('room_%d.log' % id, 'a')
        self._log('====================\nServer started')
        self._requestid = 0

        self.id = id
        self.players = []
        self.spectators = []
        self.idle_users = set()
        self.player_names = {}
        self._ruleset = copy.deepcopy(DEFAULT_RULESET)
        self._test = False
        self._ai = None

        self.last_active = time.monotonic()
        self.last_chat_active = self.last_active
        self.private = False
        self._reset(new_game=True)

    def _dispose(self):
        if self._ai:
            self._ai.dispose()
            self._ai = None
        for user in self._users:
            if user not in sessions:
                continue
            sessions[user].leave(self.id, None)
        self.players = []
        self.spectators = []
        self.clear_events()

    @property
    def _users(self):
        return self.players + self.spectators

    @property
    def _month(self):
        if len(self.players) < 2:
            if len(self.scores[0]) > 0:
                return self._ruleset['months'] + 1
            return 0
        return len(self.scores[0]) + 1

    @property
    def _gameover(self):
        return self._month < 1 or self._month > self._ruleset['months']

    def _is_expired(self, now):
        if len(self.players) == 1 and self.players[0] == 'ai':
            return True
        if self.last_active < now - 300:
            return True
        if not self._gameover:
            return False
        if self.last_active < now - 60:
            return True
        return self._month > self._ruleset['months'] and self.last_chat_active < now - 900

    def test___cheat(self, player, mode):
        import os
        if os.getenv('ENABLE_CHEAT', False) and player in self.players:
            self._test = (1 - self._player(player), mode)

    def _log(self, msg):
        if self.logfile:
            self.logfile.write(msg + '\n')
            self.logfile.flush()

    def _reset(self, new_game=False):
        self.last_chat_active = time.monotonic()
        if new_game or not self.board:
            self.scores = [[], []]
            self.board = BoardState(self._ruleset)
        else:
            self.board = BoardState(self._ruleset, self.board.state[1])
        self.did_koikoi = [False, False]
        self._update_lobby()

    def _update_lobby(self):
        from . import lobby
        lobby.update_room(self)

    def rematch(self, player):
        if player not in self.players or len(self.players) < 2:
            return { 'ok': False }
        self._start()

    def _start(self):
        if self.board.state[0] == 'finish':
            self._reset(True)
        self.board.determine_first()
        self._deal()
        self._dispatch('update', state=self.board_state)

    def _deal(self):
        must_deal = self._month <= self._ruleset['months']
        self._dispatch('deal')
        while must_deal:
            must_deal = False
            self.board.deal(test=self._test)
            if self._test:
                self._dispatch('message', src='System', msg='Note: debugging cheat enabled.')
            self._test = False
            winner, yaku = self.board.check_initial()
            if winner == -1:
                self._dispatch('message', src='System', msg='Dealt %s to table, restarting round...' % yaku[0]['name'])
                self.board.void_round()
                must_deal = True
            elif winner == 'draw':
                self._dispatch('message', src='System', msg='Dealt %s to %s.' % (yaku[0]['name'], self._get_name(self.players[0])))
                self._dispatch('message', src='System', msg='Dealt %s to %s.' % (yaku[1]['name'], self._get_name(self.players[1])))
                self._dispatch('message', src='System', msg='Round is a draw.')
                self.scores[0].append(0)
                self.scores[1].append(0)
                self.board.void_round(True)
                must_deal = True
            elif winner is not False:
                score = yaku[0]['points']
                self._dispatch(
                    'yaku',
                    player=winner,
                    score=score,
                    yaku=yaku,
                )
                self.board.auto_win(winner, score, yaku)

    def _player(self, player):
        if player in self.players:
            return self.players.index(player)
        if player in self.spectators:
            return self.spectators.index(player) + 2
        return len(self._users) + 1

    def _dispatch(self, eventName, **kwargs):
        self._log(json.dumps({ 'eventName': eventName, **kwargs }, default=lambda v: None))
        if eventName == 'update' and self._ai:
            self._ai.update(self.board_state('ai'))
        self.dispatch_event(eventName, **kwargs)

    def _get_name(self, id):
        if not id:
            return None
        if id == 'ai':
            return 'AI'
        session = sessions.get(id)
        if session:
            self.player_names[id] = session.name
            return session.name
        elif id in self.player_names:
            return self.player_names[id]
        else:
            return 'unknown user'

    def _get_idle_users(self):
        return [{ 'name': self._get_name(id), 'id': self._player(id) } for id in self.idle_users]

    def _idle(self, player):
        if player not in self.idle_users:
            self.idle_users.add(player)
            self.dispatch_event('idle', users=self._get_idle_users())

    def _remove_idle(self, player):
        if player in self.idle_users:
            self.idle_users.remove(player)
            return len(self.idle_users) == 0
        return False

    def set_name(self, player, name):
        if player == current_session().cookie:
            is_user = player in self._users
            if current_session().name != name or not is_user:
                old_name = current_session().name
                current_session().name = name
                if not is_user:
                    if len(self.players) > 0:
                        self.spectate(player)
                    else:
                        self.join(player)
                self._update_lobby()
                self._update()
                if is_user and old_name != name:
                    self._dispatch('renamed', name=old_name, newName=name)
            return { 'ok': True }

    def spectate(self, player):
        if player in self.players and self._month >= 1 and self._month <= 12:
            self.players.remove(player)
        if player not in self._users:
            self.spectators.append(player)
            self._dispatch('spectating', name=self._get_name(player))
            self._update_lobby()
        raise RedirectException('/room/%d' % self.id)

    def join(self, player):
        if player not in self.players and len(self.players) < 2:
            if player in self.spectators:
                self.spectators.remove(player)
            self.players.append(player)
            if len(self.players) == 2:
                self._start()
            self._dispatch('joined', name=self._get_name(player))
            self._update_lobby()
            raise RedirectException('/room/%d' % self.id)
        raise RedirectException('/lobby')

    def leave(self, player):
        if player in self._users:
            showAbort = False
            if player in self.spectators:
                self.spectators.remove(player)
            if player in self.players:
                if len(self.players) and not self._gameover and self.board.state[0] not in ('game_start', 'finish'):
                    self.board.end_game(self._player(player))
                    showAbort = True
                self.players.remove(player)
                self._update()
            self._dispatch('left', name=self._get_name(player))
            if len(self.players) == 1 and self.players[0] == 'ai':
                self.players = []
                self._dispatch('left', name='AI')
            if len(self.players) != 2:
                self.scores = [[], []]
            if showAbort:
                self._dispatch('message', src='System', msg='The game was aborted because a player left.')
            self._update_lobby()
        raise RedirectException('/lobby')

    def message(self, player, msg=False, src=False):
        if not src and player in self._users:
            src = self._get_name(player)
        if not src or not msg:
            return { "ok": False }
        self.last_chat_active = time.monotonic()
        if self._ai and msg.startswith('/ai'):
            self._ai.command(player, msg[4:])
        else:
            self._dispatch('message', src=src, msg=msg)
        return { "ok": True }

    def match(self, player, hand, field):
        self.board.match(self._player(player), int(hand), int(field))
        self._dispatch('animate', action='match', card=int(hand), field=int(field), source=player)
        self._update()

    def discard(self, player, hand):
        self.board.discard(self._player(player), int(hand))
        self._dispatch('animate', action='discard', card=int(hand), source=player)
        self._update()

    def draw_table(self, player):
        p = self._player(player)
        card = self.board.faceup.index
        self.board.draw_table(p)
        self._dispatch('animate', action='discard', card=int(card), source='deck')
        self._report_yaku(p)
        self._update()

    def draw_match(self, player, field):
        p = self._player(player)
        card = self.board.faceup.index
        self.board.match(p, card, int(field))
        self._dispatch('animate', action='match', card=int(card), field=int(field), source='deck')
        self._report_yaku(p)
        self._update()

    def _report_yaku(self, p):
        if (self.board.state[0] == 'koikoi?' or self.board.state[0] == 'continue?') and self.board.state[1] == p:
            score, yaku = HanafudaYaku(self.board.matches[p]).compute_score(self.did_koikoi[1 - p])
            if score > 0:
                self._dispatch(
                    'yaku',
                    player=p,
                    score=score,
                    yaku=[{
                        'name': y['name'],
                        'points': y['points'],
                        'cards': [c.index for c in y['cards']],
                    } for y in yaku],
                )
                if not len(self.board.hands[p]):
                    self.board.auto_win(p)
                return
        self._check_draw()

    def _check_draw(self):
        if not len(self.board.hands[0]) and not len(self.board.hands[1]):
            self._dispatch('round_draw')
            self._win_round(self.board.dealer, 0)

    def koikoi(self, player):
        p = self._player(player)
        if not self._ruleset['multiKoikoi'] and self.did_koikoi[p]:
            self.shobu(player)
            return
        self.board.koikoi(p)
        self.did_koikoi[p] = True
        self.message(player, 'Koi-koi!')
        self._update()

    def shobu(self, player):
        p = self._player(player)
        self.board.shobu(p)
        if self.board.auto_score:
            score = self.board.auto_score
        else:
            score, _ = HanafudaYaku(self.board.matches[p]).compute_score(self.did_koikoi[1 - p])
        self.message(player, 'Shobu!')
        self._win_round(p, score)

    def _win_round(self, p, score):
        self.scores[p].append(score)
        self.scores[1 - p].append(0)
        self._reset()
        if self._month <= self._ruleset['months']:
            self._deal()
        else:
            self.board.end_game()
        self._update()

    def ruleset(self, _, months=None, vsai=None):
        if months:
            self._ruleset['months'] = int(months)
            raise RedirectException('/room/%s' % self.id)
        if vsai and len(self.players) == 1:
            self._ruleset['vsAI'] = True
            self._ai = AIPlayer(self)
            self.join('ai')
            raise RedirectException('/room/%s' % self.id)
        return self._ruleset

    def _update(self):
        self._log(json.dumps(self.board_state(2)))
        self._dispatch('update', state=self.board_state)

    def board_state(self, player):
        player = self._player(player)
        faceup = None
        if self.board.faceup:
            faceup = self.board.faceup.index
        state = {
            'whoami': player,
            'month': self._month,
            'dealer': self.board.dealer,
            'deck': len(self.board.deck),
            'field': [card.index for card in self.board.field],
            'faceup': faceup,
            'scores': [sum(self.scores[0]), sum(self.scores[1])],
            'users': [self._get_name(id) for id in (self.players + [None, None])[:2] + self.spectators],
            'idle': self._get_idle_users(),
            'mode': self.board.state[0],
            'turn': self.board.state[1],
        }
        if player > 1:
            # spectator
            state.update({
                'hand': len(self.board.hands[0]),
                'matches': [card.index for card in self.board.matches[0]],
                'opponentHand': len(self.board.hands[1]),
                'opponentMatches': [card.index for card in self.board.matches[1]],
                'mode': 'spectator',
            })
        else:
            state.update({
                'hand': [card.index for card in self.board.hands[player]],
                'matches': [card.index for card in self.board.matches[player]],
                'opponentHand': len(self.board.hands[1 - player]),
                'opponentMatches': [card.index for card in self.board.matches[1 - player]],
            })
        if state['mode'] == 'koikoi?' or state['mode'] == 'continue?':
            scorer = self.board.state[1]
            if self.board.auto_score:
                score = self.board.auto_score
                yaku = self.board.auto_yaku
            else:
                score, yaku = HanafudaYaku(self.board.matches[scorer]).compute_score(self.did_koikoi[1 - scorer])
                yaku = [{
                    'name': y['name'],
                    'points': y['points'],
                    'cards': [c.index for c in y['cards']],
                } for y in yaku]
            state['prompt'] = {
                'player': scorer,
                'score': score,
                'yaku': yaku,
            }
        return state

@app.route('/<int:id>')
def render_room(id):
    if id not in rooms:
        raise RedirectException('/lobby')
    room = rooms[id]
    sess = current_session(False)
    if sess and sess.cookie and sess.name:
        player = sess.cookie
        if player in sessions:
            sessions[player].join(id, None)
        if player not in room._users:
            room.spectate(player)
    return index_html(('room.js',))

def on_ws_disconnect(ws, id):
    ws.session.leave(id, ws)
    connections = ws.session.rooms.get(id, [])
    if not connections and id in rooms:
        rooms[id]._idle(ws.session.cookie)

def on_ws_connect(ws, id):
    if id not in rooms:
        raise RedirectException('/lobby')
    rooms[id]._remove_idle(ws.session.cookie)
    rooms[id]._update()
    ws.session.join(id, ws)

@wsapp.on_connect(on_ws_connect)
@wsapp.on_disconnect(on_ws_disconnect)
@app.route('/<int:id>/<string:fn>')
@app.route('/<int:id>', websocket=True)
def send_to_room(id, fn, **args):
    requestid = None
    room = rooms.get(id, None)
    try:
        if fn.startswith('_') or not hasattr(Room, fn) or type(getattr(Room, fn)).__name__ != 'function':
            raise Exception('invalid function')
        if not room or room._is_expired(time.monotonic()):
            raise RedirectException('/lobby')
        room.last_active = time.monotonic()
        if not args and not is_websocket_context():
            args = dict((k, v[0] if isinstance(v, list) else v) for k, v in flask.request.args.items())
        session = current_session()
        player = session.cookie
        print('room#%d [%s] %s(%s)' % (id, session.name, fn, args))
        requestid = room._requestid
        room._requestid += 1
        room._log('%d\t%s\t%s\t%s\n' % (requestid, player, fn, json.dumps(args)))
        idle_clear = room._remove_idle(player)
        if idle_clear:
            room._update()
        with room:
            response = getattr(room, fn)(player, **args)
        if not response:
            response = { 'ok': True }
        room.logfile.write('%d\t\t%s\n' % (requestid, json.dumps(response),))
        room.logfile.flush()
        return json_response(response)
    except Exception as err:
        if room and room.logfile:
            room.logfile.write('%s\t\tUncaught exception: %s\n' % (requestid, err))
            traceback.print_exc(file=room.logfile)
            room.logfile.flush()
        raise

@app.route('/new')
def new_room():
    while True:
        id = random.randint(1, 1 << 31)
        if id not in rooms:
            break
    rooms[id] = Room(id)
    raise RedirectException('/room/%s/join' % id)
