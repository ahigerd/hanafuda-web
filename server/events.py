import threading
import time
import uwsgi
from . import sessions

class EventGroup:
    def __init__(self, room, timeout=30.0):
        self._room = room
        self._timeout = timeout
        self._timeout_us = timeout * 1000000
        self._last_timestamp = 0
        self._fds = []
        self._history = []
        self._pending = {}
        self._threadlocal = threading.local()

    def __enter__(self):
        if hasattr(self._threadlocal, 'stack'):
            self._threadlocal.stack += 1
        else:
            self._threadlocal.stack = 1
            self._threadlocal.queue = []

    def __exit__(self, exc, value, traceback):
        self._threadlocal.stack -= 1
        if self._threadlocal.stack == 0 and len(self._threadlocal.queue):
            self._history += self._threadlocal.queue
            self._threadlocal.queue = []
            self._notify_all()

    def _e_enqueue(self, eventName, **kwargs):
        event = {
            'eventName': eventName,
            'timestamp': self._now(),
            **kwargs,
        }
        self._threadlocal.queue.append(event)

    def _e_dispatch(self, eventName, **kwargs):
        event = {
            'eventName': eventName,
            'timestamp': self._now(),
            **kwargs,
        }
        self._history.append(event)
        self._notify_all()

    @property
    def dispatch_event(self):
        if not hasattr(self._threadlocal, 'stack'):
            self._threadlocal.stack = 0
            self._threadlocal.queue = []
        if self._threadlocal.stack > 0:
            return self._e_enqueue
        else:
            return self._e_dispatch

    def _now(self):
        ts = 0
        while self._last_timestamp >= ts:
            ts = int(time.monotonic() * 1000000)
        self._last_timestamp = ts
        return ts

    def _notify_all(self, force=False):
        stale = self._last_timestamp - (self._timeout_us * 3)
        history = []
        fn_keys = set()
        for i, event in enumerate(self._history):
            if event['timestamp'] < stale:
                continue
            if 'dedupe' in event:
                has_dupe = False
                for j in range(i + 1, len(self._history)):
                    if 'dedupe' in self._history[j] and self._history[j]['dedupe'] == event['dedupe']:
                        has_dupe = True
                        break
                if has_dupe:
                    continue
            history.append(event)
            for key, value in event.items():
                if callable(value):
                    fn_keys.add(key)
        self._history.clear()
        if not force and not len(history):
            return
        for session in sessions.sessions.values():
            if self._room in session.rooms:
                for event in history:
                    if fn_keys:
                        for key in fn_keys:
                            if key in event:
                                event = {
                                    **event,
                                    key: event[key](session.cookie)
                                }
                    print('emit to', session.name, self._room, event, session.rooms[self._room])
                    session.emit(event, self._room)

    def clear_events(self):
        self._history = []
        self._notify_all()
