_animals = [7, 15, 19, 23, 27, 30, 35, 39, 42]
_brights = [3, 11, 31, 43, 47]
_red_tanzaku = [2, 6, 10]
_blue_tanzaku = [22, 34, 38]
_tanzaku = _red_tanzaku + _blue_tanzaku + [14, 18, 26, 41]
_non_chaff = _animals + _brights + _tanzaku
_chaff = list(i for i in range(48) if i == 35 or i not in _non_chaff)

class HanafudaCard:
    def __init__(self, index):
        self.index = index
        self.month = index // 4
        if index in _brights:
            self.value = 20
        elif index in _animals:
            self.value = 10
        elif index in _tanzaku:
            self.value = 5
        else:
            self.value = 1

    def __str__(self):
        return '[card %s-%s]' % (self.month, self.index)

    @property
    def animal(self):
        return self.index in _animals

    @property
    def bright(self):
        return self.index in _brights

    @property
    def red_tanzaku(self):
        return self.index in _red_tanzaku

    @property
    def blue_tanzaku(self):
        return self.index in _blue_tanzaku

    @property
    def tanzaku(self):
        return self.index in _tanzaku

    @property
    def chaff(self):
        return self.index in _chaff

Cards = list(HanafudaCard(i) for i in range(48))

ANIMALS = list(Cards[i] for i in _animals)
BRIGHTS = list(Cards[i] for i in _brights)
RED_TANZAKU = list(Cards[i] for i in _red_tanzaku)
BLUE_TANZAKU = list(Cards[i] for i in _blue_tanzaku)
TANZAKU = list(Cards[i] for i in _tanzaku)
RAIN = Cards[43]
CUP = Cards[35]
MOON = Cards[31]
CURTAIN = Cards[11]
BOAR = Cards[27]
DEER = Cards[39]
BUTTERFLY = Cards[23]
CHAFF = list(Cards[i] for i in _chaff)
