import werkzeug.exceptions as wz_exceptions
import json
import os
import scss
import flask
import glob

MIME_TYPES = {
    "js": "text/javascript",
    "css": "text/css",
    "svg": "image/svg+xml",
    "png": "image/png",
    "wav": "audio/wav",
    "mp3": "audio/mpeg",
}

base_folder = os.path.abspath(os.path.join(os.path.dirname(__file__), '../static'))
def static_path(path):
    return os.path.join(base_folder, path)

def mtime(path):
    return os.stat(static_path(path)).st_mtime

def static_file(fn):
    if '..' in fn:
        raise wz_exceptions.NotFound()
    try:
        fullpath = static_path(fn)
        cb = flask.request.args.get('cb')
        ext = fn.split('.')[-1]
        if not cb and ext in ('js', 'css'):
            return flask.redirect('%s?cb=%d' % (flask.request.base_url, mtime(fullpath)), code=307)
        with open(fullpath, 'rb') as f:
            resp = flask.Response(f.read())
            resp.headers['Content-Type'] = MIME_TYPES.get(ext, 'application/octet-stream')
            resp.headers['Access-Control-Allow-Origin'] = '*'
            return resp
    except FileNotFoundError as e:
        raise wz_exceptions.NotFound()

# The first entry in `css_files` is the entry point.
# Additional entries are checked for timestamps but not explicitly loaded.
# Use the `@import` directive to import other files.
css_files = ['hanafuda.scss']
js_files = [os.path.basename(fn) for fn in glob.iglob(os.path.join(base_folder, '*.js'))]
html_file = static_path('index.html')

PREFIX = ''

class ContentCache:
    def __init__(self):
        self._cache = {}

    def get(self, key, check_files = None):
        if not check_files:
            check_files = [key]
        updated = max(mtime(path) for path in check_files)
        cache_time, content = self._cache.get(key, (0, None))
        if updated > cache_time:
            content = self.load(key)
            self._cache[key] = (updated, content)
        return content

    def last_updated(self, key, check_files = None):
        self.get(key, check_files)
        return self._cache[key][0]

    def load(self, key):
        return open(static_path(key), 'rb').read()

class JsonCache(ContentCache):
    def load(self, key):
        return json.loads(open(static_path(key), 'rb').read())

class CssCache(ContentCache):
    def load(self, key):
        return scss.compiler.compile_string('@import "%s";' % key, search_path=[static_path('')])

class IndexHtmlCache(ContentCache):
    def load(self, load_scripts):
        # TODO: consider using jinja instead of doing this by hand
        theme = flask.request.cookies.get('hf_theme', None)
        if theme == 'default' or not theme:
            stylesheets = '<link rel="stylesheet" type="text/css" href="%s/style.css?cb=%d" />' % (PREFIX, css_cache.last_updated(css_files[0]))
        else:
            stylesheets = '<link rel="stylesheet" type="text/css" href="%s/style/%s.css?cb=%d" />' % (PREFIX, theme, css_cache.last_updated(css_files[0]))
        scripts = []
        for path in js_files:
            url = "%s/static/%s" % (PREFIX, path)
            if path in load_scripts:
                scripts.append('<script type="module" src="%s"></script>' % url)
            else:
                scripts.append('<link rel="prefetch" as="script" type="text/javascript" href="%s" crossorigin="use-credentials" />' % url)
        return open(html_file).read().replace('{STYLESHEETS}', stylesheets).replace('{SCRIPTS}', '\n'.join(scripts))

css_cache = CssCache()
html_cache = IndexHtmlCache()

def style_css(theme = None):
    if theme:
        root_file = theme + '.scss'
        check_files = css_files + [root_file]
    else:
        root_file = css_files[0]
        check_files = css_files
    try:
        content = css_cache.get(root_file, check_files)
        return content, { 'content-type': 'text/css' }
    except Exception as e:
        if not theme:
            raise
        return style_css(None)

def index_html(load_scripts=tuple()):
    theme = flask.request.cookies.get('hf_theme', None)
    style_css(theme)
    return html_cache.get(load_scripts + (theme,), css_files + [html_file])

def json_response(data):
    return json.dumps(data), { 'content-type': 'application/json' }

class RedirectException(Exception):
    def __init__(self, url, status_code=307):
        super(Exception, self).__init__('redirecting to %s' % url)
        self.url = url
        self.status_code = status_code

def register_content(app, prefix):
    global PREFIX
    PREFIX = prefix
    app.route(prefix + '/static/<path:fn>')(static_file)
    app.route(prefix + '/style.css')(style_css)
    app.route(prefix + '/style/<path:theme>.css')(style_css)
